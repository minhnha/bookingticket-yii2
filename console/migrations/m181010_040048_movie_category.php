<?php

use yii\db\Migration;

/**
 * Class m181010_040048_movie_category
 */
class m181010_040048_movie_category extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%movie_category}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_created_by_movie_category_user','{{%movie_category}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_movie_category_user','{{%movie_category}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%movie_category}}');
    }
}

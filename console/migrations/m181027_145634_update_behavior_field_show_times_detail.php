<?php

use yii\db\Migration;

/**
 * Class m181027_145634_update_behavior_field_show_times_detail
 */
class m181027_145634_update_behavior_field_show_times_detail extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%show_times_detail}}','created_at', $this->integer(11)->notNull());
        $this->addColumn('{{%show_times_detail}}','created_by', $this->integer(11)->notNull());
        $this->addColumn('{{%show_times_detail}}','updated_at', $this->integer(11)->notNull());
        $this->addColumn('{{%show_times_detail}}','updated_by', $this->integer(11)->notNull());

        $this->addForeignKey('fk_created_by_show_times_detail_user','{{%show_times_detail}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_show_times_detail_user','{{%show_times_detail}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%show_times_detail}}');
    }
}

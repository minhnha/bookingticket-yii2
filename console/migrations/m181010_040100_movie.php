<?php

use yii\db\Migration;

/**
 * Class m181010_040100_movie
 */
class m181010_040100_movie extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%movie}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string(100)->notNull(),
            'image' => $this->string(255),
            'cast' => $this->string(100),
            'director' => $this->string(50),
            'studio' => $this->string(50),
            'length' => $this->smallInteger(),
            'content' => $this->text(),
            'status' => $this->tinyInteger(),
            'cat_id' => $this->integer(11)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_cat_id_movie_movie_category','{{%movie}}','cat_id','{{%movie_category}}','id');
        $this->addForeignKey('fk_created_by_movie_user','{{%movie}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_movie_user','{{%movie}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%movie}}');
    }
}

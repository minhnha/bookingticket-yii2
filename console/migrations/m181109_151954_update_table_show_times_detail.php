<?php

use yii\db\Migration;

/**
 * Class m181109_151954_update_table_show_times_detail
 */
class m181109_151954_update_table_show_times_detail extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->addColumn('{{%show_times_detail}}', 'show_times_id', $this->integer(11)->notNull()->after('cine_complex_id'));
        $this->addColumn('{{%show_times_detail}}', 'playtime_id', $this->integer(11)->notNull()->after('movie_id'));
        $this->addForeignKey('fk_show_times_id_show_times_detail_show_times','{{%show_times_detail}}','show_times_id','{{%show_times}}','id');
        $this->addForeignKey('fk_playtime_id_show_times_detail_playtime','{{%show_times_detail}}','playtime_id','{{%playtime}}','id');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        echo "m181109_151954_update_table_show_times_detail cannot be reverted.\n";

        return false;
    }

}

<?php

use yii\db\Migration;

/**
 * Class m181010_040132_seat
 */
class m181010_040131_seat_line extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%seat_line}}', [
            'id' => $this->primaryKey(),
            'line' => $this->string(11)->notNull(),
            'seat_quantity' => $this->integer(11)->defaultValue(12)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%seat_line}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m181108_003703_slide
 */
class m181108_003703_slide extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%slide}}', [
            'id' => $this->primaryKey(),
            'link' => $this->string()->notNull(),
            'page_id' => $this->integer(11)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_page_id_slide_page','{{%slide}}','page_id','{{%slide}}','id');
        $this->addForeignKey('fk_created_by_slide_user','{{%slide}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_slide_user','{{%slide}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%slide}}');
    }
}

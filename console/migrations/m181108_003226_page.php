<?php

use yii\db\Migration;

/**
 * Class m181108_003226_page
 */
class m181108_003226_page extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%page}}', [
            'id' => $this->primaryKey(),
            'title' => $this->string()->notNull()->unique(),
            'slug' => $this->string()->notNull()->unique(),
            'content' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext')->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_created_by_page_user','{{%page}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_created_by_page_user','{{%page}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%page}}');
    }
}

<?php

use yii\db\Migration;

/**
 * Class m181010_040206_show_times_detail
 */
class m181010_040206_show_times_detail extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%show_times_detail}}', [
            'id' => $this->primaryKey(),

            'movie_id' => $this->integer(11)->notNull(),
            'date' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_movie_id_show_times_detail_movie','{{%show_times_detail}}','movie_id','{{%movie}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%show_times_detail}}');
    }
}

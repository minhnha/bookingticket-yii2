<?php

use yii\db\Migration;

/**
 * Class m181010_040206_show_times_detail
 */
class m181010_040200_show_times extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%show_times}}', [
            'id' => $this->primaryKey(),
            'date' => $this->integer(11)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_created_by_show_times_user','{{%show_times}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_show_times_user','{{%show_times}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%show_times}}');
    }
}

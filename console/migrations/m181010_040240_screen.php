<?php

use yii\db\Migration;

/**
 * Class m181010_040240_screen
 */
class m181010_040240_screen extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%screen}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->notNull(),
            'normal_seat_quantity' => $this->integer(11)->null(),
            'vip_seat_quantity' => $this->integer(11)->null(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_created_by_screen_user','{{%screen}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_screen_user','{{%screen}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%screen}}');
    }
}

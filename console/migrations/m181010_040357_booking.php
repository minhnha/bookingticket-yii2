<?php

use yii\db\Migration;

/**
 * Class m181010_040357_booking
 */
class m181010_040357_booking extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%booking}}', [
            'id' => $this->primaryKey(),
            'full_name' => $this->string(100)->notNull(),
            'phone_number' => $this->string(11)->notNull(),
            'seat_code' => $this->string(100)->notNull(),
            'complex_name' => $this->string(100)->notNull(),
            'screen_name' => $this->string(100)->notNull(),
            'show_date' => $this->integer(11)->notNull(),
            'show_time' => $this->integer(11)->notNull(),
            'movie_title' => $this->integer(11)->notNull(),
            'normal_ticket' => $this->integer(11)->notNull(),
            'vip_ticket' => $this->integer(11)->notNull(),
            'price' => $this->integer(11)->notNull(),
            'description' => $this->string(255)->null(),
            'status' => $this->tinyInteger()->defaultValue(0),

            'user_id' => $this->integer(11)->notNull(),
            'show_times_id' => $this->integer(11)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_movie_id_booking_movie','{{%booking}}','movie_id','{{%movie}}','id');
        $this->addForeignKey('fk_user_id_booking_user','{{%booking}}','user_id','{{%user}}','id');
        $this->addForeignKey('fk_seat_id_booking_movie','{{%booking}}','seat_id','{{%seat}}','id');
        $this->addForeignKey('fk_cinema_complex_id_booking_cinema_complex','{{%booking}}','cinema_complex_id','{{%cinema_complex}}','id');
        $this->addForeignKey('fk_screen_id_booking_screen','{{%booking}}','screen_id','{{%screen}}','id');
        $this->addForeignKey('fk_show_times_id_booking_show_times','{{%booking}}','show_times_id','{{%show_times_detail}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%booking}}');
    }
}

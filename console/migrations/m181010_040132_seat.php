<?php

use yii\db\Migration;

/**
 * Class m181010_040132_seat
 */
class m181010_040132_seat extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%seat}}', [
            'id' => $this->primaryKey(),
            'seat_code' => $this->integer(11)->notNull(),
            'status' => $this->tinyInteger()->defaultValue(0),
            'cat_id' => $this->integer(11)->notNull(),
            'seat_line_id' => $this->integer(11)->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_cat_id_seat_seat_category','{{%seat}}','cat_id','{{%seat_category}}','id');
        $this->addForeignKey('fk_seat_line_id_seat_seat_line','{{%seat}}','seat_line_id','{{%seat_line}}','id');
        $this->addForeignKey('fk_created_by_seat_user','{{%seat}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_seat_user','{{%seat}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%seat}}');
    }
}

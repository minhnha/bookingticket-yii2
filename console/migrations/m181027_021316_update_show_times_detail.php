<?php

use yii\db\Migration;

/**
 * Class m181027_021316_update_show_times_detail
 */
class m181027_021316_update_show_times_detail extends Migration
{
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->addColumn('{{%show_times_detail}}','cine_complex_id', $this->integer(11)->notNull()->after('id'));
        $this->addColumn('{{%show_times_detail}}','screen_id', $this->integer(11)->notNull()->after('show_times_id'));
        $this->addColumn('{{%show_times_detail}}','start_time', $this->integer(11)->notNull()->after('movie_id'));
        $this->addColumn('{{%show_times_detail}}','end_time', $this->integer(11)->notNull()->after('start_time'));
        $this->addColumn('{{%show_times_detail}}','normal_seat_remaining', $this->integer(11)->notNull()->after('end_time'));
        $this->addColumn('{{%show_times_detail}}','vip_seat_remaining', $this->integer(11)->notNull()->after('normal_seat_remaining'));

        $this->addForeignKey('fk_cine_complex_id_show_times_detail_cinema_complex','{{%show_times_detail}}','cine_complex_id','{{%cinema_complex}}','id');
        $this->addForeignKey('fk_screen_id_show_times_detail_screen','{{%show_times_detail}}','screen_id','{{%screen}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%show_times_detail}}');
    }

}

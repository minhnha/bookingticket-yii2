<?php

use yii\db\Migration;

/**
 * Class m181109_150643_add_version_column_in_movie
 */
class m181109_150643_add_version_column_in_movie extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->addColumn('{{%movie}}', 'version',  $this->tinyInteger()->notNull()->after('length'));
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        echo "m181109_150643_add_version_column_in_movie cannot be reverted.\n";

        return false;
    }

}

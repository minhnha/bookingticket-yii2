<?php

use yii\db\Migration;

/**
 * Class m181010_034556_user
 */
class m181010_034556_user extends Migration
{
     public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            'id' => $this->primaryKey(),
            'username' => $this->string()->notNull()->unique(),
            'full_name' => $this->string(255)->notNull(),
            'auth_key' => $this->string(32)->notNull(),
            'password_hash' => $this->string()->notNull(),
            'password_reset_token' => $this->string()->unique(),
            'email' => $this->string()->notNull()->unique(),
            'gender' => $this->tinyInteger()->null(),
            'birthday' => $this->integer(11)->null(),
            'address' => $this->string(255)->null(),
            'phone_number' => $this->string(11)->null(),
            'permission' => $this->integer()->notNull()->defaultValue(0),

            'is_active' => $this->smallInteger()->notNull()->defaultValue(0),
            'active_token' => $this->string(40)->unique(),
            'created_at' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
        ], $tableOptions);
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}

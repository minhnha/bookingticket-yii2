<?php

use yii\db\Migration;

/**
 * Class m181111_113430_add_fk_cine_complex_in_table_screen
 */
class m181111_113430_add_fk_cine_complex_in_table_screen extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function Up()
    {
        $this->addColumn('{{%screen}}', 'cine_complex_id', $this->integer(11)->notNull()->after('vip_seat_quantity'));

        $this->addForeignKey('fk_cine_complex_id_screen_cinema_complex', '{{%screen}}', 'cine_complex_id', '{{%cinema_complex}}', 'id');
    }

    /**
     * {@inheritdoc}
     */
    public function Down()
    {
        echo "m181111_113430_add_fk_cine_complex_in_table_screen cannot be reverted.\n";

        return false;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m181111_113430_add_fk_cine_complex_in_table_screen cannot be reverted.\n";

        return false;
    }
    */
}

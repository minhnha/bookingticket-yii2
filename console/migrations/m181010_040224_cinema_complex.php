<?php

use yii\db\Migration;

/**
 * Class m181010_040224_cinema_complex
 */
class m181010_040224_cinema_complex extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%cinema_complex}}', [
            'id' => $this->primaryKey(),
            'name' => $this->string(100)->unique()->notNull(),
            'address' => $this->string(255)->unique()->notNull(),
            'phone_number' => $this->char(100)->unique()->notNull(),

            'created_at' => $this->integer(11)->notNull(),
            'created_by' => $this->integer(11)->notNull(),
            'updated_at' => $this->integer(11)->notNull(),
            'updated_by' => $this->integer(11)->notNull(),
        ], $tableOptions);
        $this->addForeignKey('fk_created_by_cinema_complex_user','{{%cinema_complex}}','created_by','{{%user}}','id');
        $this->addForeignKey('fk_updated_by_cinema_complex_user','{{%cinema_complex}}','updated_by','{{%user}}','id');
    }

    public function down()
    {
        $this->dropTable('{{%cinema_complex}}');
    }
}

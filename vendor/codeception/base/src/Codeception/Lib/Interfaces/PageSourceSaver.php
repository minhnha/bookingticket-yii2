<?php
namespace Codeception\Lib\Interfaces;

interface PageSourceSaver
{
    /**
     * Saves slide source of to a file
     *
     * ```php
     * $this->getModule('{{MODULE_NAME}}')->_savePageSource(codecept_output_dir().'slide.html');
     * ```
     * @api
     * @param $filename
     */
    public function _savePageSource($filename);
}

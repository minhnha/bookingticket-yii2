<?php

namespace frontend\models;

use common\models\CinemaComplex;
use common\models\Movie;
use common\models\Screen;
use common\models\ShowTimes;
use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShowTimesDetail;

/**
 * ShowTimesDetailSearch represents the model behind the search form about `common\models\ShowTimesDetail`.
 */
class ShowTimesDetailSearch extends ShowTimesDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'normal_seat_remaining', 'vip_seat_remaining'], 'integer'],
            [['cine_complex_id', 'show_times_id', 'screen_id', 'movie_id', 'playtime_id'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShowTimesDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        $query->joinWith('screen');
        $query->joinWith('showTimes');
        $query->joinWith('cineComplex');
        $query->joinWith('movie');
        $query->joinWith('playtime');

        // grid filtering conditions
        $query->andFilterWhere([
            //'id' => $this->id,
            'normal_seat_remaining' => $this->normal_seat_remaining,
            'vip_seat_remaining' => $this->vip_seat_remaining,
        ]);

        $query->andFilterWhere(['like', Screen::tableName().'.name', $this->screen_id])
            ->andFilterWhere(['like', 'FROM_UNIXTIME(`date`, "%d/%m/%Y")', $this->show_times_id])
            ->andFilterWhere(['like', CinemaComplex::tableName().'.name', $this->cine_complex_id])
            ->andFilterWhere(['like', Movie::tableName().'.title', $this->movie_id])
            ->andFilterWhere([
                'OR',
                ['like', 'FROM_UNIXTIME(`end_time`+3600, "%H:%i")', $this->playtime_id],
                ['like', 'FROM_UNIXTIME(`start_time`+3600, "%H:%i")', $this->playtime_id]
            ]);

        return $dataProvider;
    }
}

$(document).ready(function(){
    // when click button Register
    $('.modalButtonRegister').click(function(){
        $('#modal-register').modal('show')
            .find('#modalRegisterContent')
            .load($(this).attr('value'));
    });

    // when click button Login
    $('.modalButtonLogin').click(function(){
        $('#modal-login').modal('show')
            .find('#modalLoginContent')
            .load($(this).attr('value'));
    });

});

<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/16/2018
 * Time: 9:43 AM
 */

namespace frontend\controllers;

use Yii;
use common\models\User;
use common\models\booking;
use yii\web\Controller;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

class UserController extends Controller{

    public function actionIndex($id){

        $model = User::findOne(['id' => $id]);
        $booking = Booking::find()->where(['full_name' => $model->full_name])->all();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }

        if ($model->load(Yii::$app->request->post())) {
            if($model->password != null){
                $model->updatePassword($model->password);
            }
            if ($model->save()){
                Yii::$app->user->logout();
                Yii::$app->session->setFlash('success', "Cập nhật tài khoản <b>$model->username</b> thành công! Vui lòng đăng nhập lại để tiếp tục sử dụng.");
                return $this->goHome();
            }
            else
                Yii::$app->session->setFlash('error', 'Lỗi: ' . print_r($model->getErrors()));
            return $this->redirect(['index?id='.$model->id]);
        }

        return $this->render('index',[
            'model' => $model,
            'booking' => $booking
        ]);
    }
}
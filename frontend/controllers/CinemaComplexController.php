<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 8:57 AM
 */
namespace frontend\controllers;

use yii\web\Controller;
use common\models\CinemaComplex;

class CinemaComplexController extends Controller
{
    public function actionIndex(){
        $listCineComplex = CinemaComplex::find()->all();
        return $this->render('index',[
            'listCineComplex' => $listCineComplex
        ]);
    }

    public function actionView(){
        return $this->render('view');
    }
}
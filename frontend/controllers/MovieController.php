<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 7:38 AM
 */
namespace frontend\controllers;

use yii\web\Controller;
use common\models\Movie;

class MovieController extends Controller
{
    public function actionIndex(){
        $listMovie = Movie::find()->all();
        return $this->render('index',[
            'listMovie' => $listMovie
        ]);
    }

    public function actionView($id){
        $model = Movie::findOne(['id' => $id]);
        return $this->render('view',[
            'model' => $model
        ]);
    }
}
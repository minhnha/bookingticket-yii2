<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 8:48 AM
 */

namespace frontend\controllers;

use Yii;
use common\models\Booking;
use common\models\BookingDetail;
use common\models\Seat;
use common\models\SeatCategory;
use common\models\ShowTimesDetail;
use yii\db\Query;
use yii\helpers\Url;
use yii\web\Controller;

ob_start();

class BookingTicketController extends Controller
{
    public function actionIndex(){
        $model = new Booking();
        return $this->render('index',[
            'model' => $model,
        ]);
    }

    public function actionView(){
        return $this->render('view');
    }

    public function actionCreate()
    {
        $model = new Booking();
        if($model->load(Yii::$app->request->post())){
            $model->pass_flag = true;
            if ($model->save())
                Yii::$app->session->setFlash('success', 'Create booking ticket successful.');
            else{
                Yii::$app->session->setFlash('error', 'Create booking ticket fail !');
            }
            return $this->redirect(['index']);
        }
        return $this->render('index', [
            'model' => $model,
        ]);
    }

    public function actionGetShowTimesByMovie($movie_id){
        $query = new Query();

        $query->select(['st.id', 'FROM_UNIXTIME(st.date, "%d/%m/%Y") date'])
            ->from(['{{%show_times_detail}} std'])
            ->join('inner join',
                '{{%show_times}} st',
                'std.show_times_id = st.id')
            ->where(['std.movie_id' => $movie_id])
            ->groupBy('st.id');
        $query->createCommand();
        $data = $query->all();

        // Data show to html
        $listDate = '';
        foreach ($data as $item):
            $listDate .= '<div class="col-md-2 mb-2">
                                <div id="'.$item['id'].'" class="date-target d-flex align-items-center justify-content-center">
                                    <span>'.$item['date'].'</span>
                                </div>
                           </div>';
        endforeach;

        // Sent to Script of Show times (date)
        $urlShowTimes = Url::to(['booking-ticket/get-show-times-by-date']);
        $script = "
            <script>
                $(document).ready(function() {         
                    $('.date-target').click(function () {
                        $('.date-target').removeClass('checked');
                        $(this).toggleClass('checked');
                        
                        let show_times_id = $(this).attr('id');
                        let movie_id = $('.movie-target').attr('id');
                        let data = {
                            show_times_id: show_times_id,
                            movie_id: movie_id
                        }
                        $.ajax({ 
                            url: '$urlShowTimes',
                            method: 'GET',
                            data: data,
                            success: function(data) {
                                $('.cine-complex-list').html(data['listCineComplex']);
                                $('.cinema-target').append(data['script']);
                                $('.btn-next').addClass('disabled');
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                alert(xhr.status);
                                alert(thrownError);
                            }
                        });
                    });
                });
            </script>
        ";

        $this->asJson([
            'listDate' => $listDate,
            'script' => $script
        ]);
    }

    public function actionGetShowTimesByDate($movie_id, $show_times_id){
        $query = new Query();
        $query ->select(['cc.id as id', 'cc.name'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%cinema_complex}} cc',
                'cc.id = std.cine_complex_id'
            )
            ->where(['std.movie_id' => $movie_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->groupBy('cc.id');
        $query->createCommand();
        $data = $query->all();

        // Data show to html
        $listCineComplex = '';
        foreach ($data as $item):
            $listCineComplex .= '<div class="col-md-2 mb-2">
                                <div id="'.$item['id'].'" class="cinema-target d-flex align-items-center justify-content-center">
                                    <span>'.$item['name'].'<span>
                                </div>
                          </div>';;
        endforeach;

        // Sent to Script of Cinema complex
        $urlCineComplex = Url::to(['booking-ticket/get-show-times-by-cine-complex']);
        $script = "
            <script>
                $(document).ready(function() {         
                    $('.cinema-target').click(function () {
                        $('.cinema-target').removeClass('checked');
                        $(this).toggleClass('checked');
                        
                        let cine_complex_id = $(this).attr('id');
                        let movie_id = $('.movie-target').attr('id');
                        let show_times_id = $('.date-target').attr('id');
                        let data = {
                            show_times_id: show_times_id,
                            movie_id: movie_id,
                            cine_complex_id: cine_complex_id
                        }
                        $.ajax({ 
                            url: '$urlCineComplex',
                            method: 'GET',
                            data: data,
                            success: function(data) {
                                $('.screen-list').html(data['listScreen']);
                                $('.screen-target').append(data['script']);
                                $('.btn-next').addClass('disabled');
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                alert(xhr.status);
                                alert(thrownError);
                            }
                        });
                    });
                });
            </script>
        ";

        $this->asJson([
            'listCineComplex' => $listCineComplex,
            'script' => $script
        ]);
    }

    public function actionGetShowTimesByCineComplex($movie_id, $show_times_id, $cine_complex_id){
        $query = new Query();
        $query ->select(['sc.id', 'sc.name'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%screen}} sc',
                'sc.id = std.screen_id'
            )
            ->where(['std.movie_id' => $movie_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->andWhere(['std.cine_complex_id' => $cine_complex_id])
            ->groupBy('std.show_times_id');
        $query->createCommand();
        $data = $query->all();

        // Data show to html
        $listScreen = '';
        foreach ($data as $item):
            $listScreen .= '<div class="col-md-2 mb-2">
                                    <div id="'.$item['id'].'" class="screen-target d-flex align-items-center justify-content-center">
                                        <span>'.$item['name'].'</span>
                                    </div>
                                </div>';
        endforeach;


        // Sent to Script of Screen
        $urlPlaytime = Url::to(['booking-ticket/get-show-times-by-playtime']);
        $script = "
            <script>
                $(document).ready(function() {         
                    $('.screen-target').click(function () {
                        $('.screen-target').removeClass('checked');
                        $(this).toggleClass('checked');
                        
                        let screen_id = $(this).attr('id');
                        let movie_id = $('.movie-target').attr('id');
                        let show_times_id = $('.date-target').attr('id');
                        let cine_complex_id = $('.cinema-target').attr('id');
                        let data = {
                            show_times_id: show_times_id,
                            movie_id: movie_id,
                            cine_complex_id: cine_complex_id,
                            screen_id: screen_id,
                        }
                        $.ajax({ 
                            url: '$urlPlaytime',
                            method: 'GET',
                            data: data,
                            success: function(data) {
                                $('.playtime-list').html(data['listPlaytime']);
                                $('.playtime-target').append(data['script']);
                                $('.btn-next').addClass('disabled');
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                alert(xhr.status);
                                alert(thrownError);
                            }
                        });
                    });
                });
            </script>
        ";

        $this->asJson([
            'listScreen' => $listScreen,
            'script' => $script
        ]);
    }

    public function actionGetShowTimesByPlaytime($movie_id, $show_times_id, $cine_complex_id, $screen_id){
        $query = new Query();

        $query->select(['pt.id', 'FROM_UNIXTIME(pt.start_time+3600, "%H:%i") start_time', 'FROM_UNIXTIME(pt.end_time+3600, "%H:%i") end_time'])
            ->from(['{{%show_times_detail}} std'])
            ->join('inner join',
                '{{%playtime}} pt',
                'std.playtime_id = pt.id')
            ->where(['std.movie_id' => $movie_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->andWhere(['std.cine_complex_id' => $cine_complex_id])
            ->andWhere(['std.screen_id' => $screen_id])
            ->groupBy('pt.id');
        $query->createCommand();
        $data = $query->all();

        // Data show to html
        $listPlaytime = '';
        foreach ($data as $item):
            $listPlaytime .= '<div class="col-md-2 mb-2">
                                <div id="'.$item['id'].'" class="playtime-target d-flex align-items-center justify-content-center">
                                    <span>'.$item['start_time'].'-'.$item['end_time'].'</span>
                                </div>
                            </div>';
        endforeach;

        $urlGetIdShowTimesDetail = Url::to(['booking-ticket/get-id-show-times-detail']);
        // Script of playtime
        $script = "
            <script>
                $(document).ready(function() {         
                    $('.playtime-target').click(function () {
                        $('.playtime-target').removeClass('checked');
                        $(this).toggleClass('checked');
                        
                        let playtime_id = $(this).attr('id');
                        let movie_id = $('.movie-target').attr('id');
                        let show_times_id = $('.date-target').attr('id');
                        let cine_complex_id = $('.cinema-target').attr('id');
                        let screen_id = $('.screen-target').attr('id');
                        let data = {
                            show_times_id: show_times_id,
                            movie_id: movie_id,
                            cine_complex_id: cine_complex_id,
                            screen_id: screen_id,
                            playtime_id: playtime_id
                        }
                        $.ajax({ 
                            url: '$urlGetIdShowTimesDetail',
                            method: 'GET',
                            data: data,
                            success: function(data) {
                                $('#idShowTimesDetail').val(data);
                            },
                            error: function(xhr, ajaxOptions, thrownError){
                                alert(xhr.status);
                                alert(thrownError);
                            }
                        });
                    });
                                
                    $('.movie-target, .cinema-target, .date-target, .screen-target, .playtime-target').click(function() {
                        if (!$('.movie-target').hasClass('checked'))
                            return false;
                        if (!$('.cinema-target').hasClass('checked'))
                            return false;
                        if (!$('.date-target').hasClass('checked'))
                            return false;
                        if (!$('.screen-target').hasClass('checked'))
                            return false;
                        if (!$('.playtime-target').hasClass('checked'))
                            return false;
                        
                        $('.btn-next').removeClass('disabled');
                    });
                });
            </script>
        ";

        $this->asJson([
            'listPlaytime' => $listPlaytime,
            'script' => $script
        ]);
    }

    public function actionGetIdShowTimesDetail($movie_id, $show_times_id, $cine_complex_id, $screen_id, $playtime_id){
        $idShowTimes = ShowTimesDetail::findOne([
            'cine_complex_id' => $cine_complex_id,
            'show_times_id' => $show_times_id,
            'screen_id' => $screen_id,
            'movie_id' => $movie_id,
            'playtime_id' => $playtime_id
        ])->id;

        $this->asJson($idShowTimes);
    }

    public function actionGetListSeatByShowTimesDetail($idShowTimeDetail){
        $booking = new Booking();
        $listSeat = $booking->getListSeatById($idShowTimeDetail);
        foreach ($listSeat as $seatLine => $items):
            echo '<div class="d-flex bd-highlight mb-2 justify-content-center">
            <ul class="list-seat-code pl-0">
                <li class="mr-5 text-center">'.$seatLine.'</li>
                <li class="p-0">
                    <ul class="sub-list p-0 text-center">
                ';
            foreach ($items as $id => $seatCode):
                $status = BookingDetail::findOne(['show_times_detail_id' => $idShowTimeDetail, 'seat_id' => $id])->status;
                ($status == 1) ? $statusSeat = 'booked' : $statusSeat = '';
                $catId = Seat::findOne(['id' => $id])->cat_id;
                $catSeat = strtolower(SeatCategory::findOne(['id' => $catId])->title);
                echo '<li class="'.$catSeat.' '.$statusSeat.'" value="'.$id.'">'.$seatLine.$seatCode.'</li>';
            endforeach;
            echo '
                    </ul>
                </li>
            </ul>
        </div>
        <!-- End list seat -->';
        endforeach;

        $urlGetPriceBySeatId = Url::to(['booking-ticket/price-by-seat-id']);
        echo "<script>
                $(document).ready(function() {
                      function formatNumber (num , unit ='') {
                            return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, \"$1,\")+unit;
                      }
                      // Checked and choose Seat
                      $('ul.sub-list li').click(function() {
                            $(this).toggleClass('checked');
                      });
                      $('.btn-SeatChecked').click(function() {
                            let hClass = $('ul.sub-list li').hasClass('checked');
                            if(hClass){
                                let ArrValue = [];
                                $('li.checked').each(function() {
                                    let value = $(this).val();
                                    ArrValue.push( value );
                                });
                                $('#seatChecked').val(ArrValue);
                                $('#chooseSeat').modal('hide');
                            }
                            
                            let movie = $('.movie-target.checked').text().trim();
                            let cinemaComplex = $('.cinema-target.checked span').text().trim();
                            let screen = $('.screen-target.checked span').text().trim();
                            let date = $('.date-target.checked span').text().trim();
                            let playtime = $('.playtime-target.checked span').text().trim();
                            
                            $('span.movie, #booking-movie_title').text(movie);
                            $('span.cinema-complex, #booking-complex_name').text(cinemaComplex);
                            $('span.date, #booking-show_date').text(date);
                            $('span.screen, #booking-screen_name').text(screen)
                            $('span.playtime, #booking-show_time').text(playtime); 
                            
                            $('#booking-movie_title').val(movie);
                            $('#booking-complex_name').val(cinemaComplex);
                            $('#booking-show_date').val(date);
                            $('#booking-screen_name').val(screen)
                            $('#booking-show_time').val(playtime); 
                            
                            let listSeatId = $('#seatChecked').val();
                            let data = { listSeatId:listSeatId };
                            $.ajax({ 
                                url: '$urlGetPriceBySeatId',
                                method: 'GET',
                                data: data,
                                success: function(data) {
                                    $('span.price-total').text(formatNumber(data['listData']['price_total'], 'VNĐ'));
                                    $('span.list-seat').text(data['listSeatLine']);
                                    
                                    $('#booking-price').val(data['listData']['price_total']);
                                    $('#booking-normal_ticket').val(data['listData']['Normal']);
                                    $('#booking-vip_ticket').val(data['listData']['Vip']);
                                    $('#booking-seat_code').val(data['listSeatLine']);
                                    let idShowTimesDetail = $('#idShowTimesDetail').val();
                                    $('#booking-show_times_detail_id').val(idShowTimesDetail);
                                    
                                },
                                error: function(xhr, ajaxOptions, thrownError){
                                    alert(xhr.status);
                                    alert(thrownError);
                                }
                            });
                            
                            $('.step-1').addClass('d-none');
                            $('.step-1').css('left', '-300px');
                            $('.step-2').removeClass('d-none').animate({right: '0px'}, 1000);
                      });
                });
            </script>";
    }

    public function actionPriceBySeatId($listSeatId){
        // get price from listSeat
        $listSeat =  array_map('intval', explode(',', $listSeatId));
        $query = new Query();
        $query ->select(['title', 'COUNT(title) qty', '(COUNT(title) * price) price'])
            ->from('{{%seat}} s')
            ->join('inner join',
                '{{%seat_category}} scat',
                'scat.id = s.cat_id'
            )
            ->where(['in', 's.id', $listSeat])
            ->groupBy('title');
        $query->createCommand();
        $data = $query->all();

        // get lineSeat from listSeat
        $query2 = New Query();
        $query2->select(['CONCAT(sl.line,s.seat_code) as list_seat_code'])
            ->from(['{{%seat}} s'])
            ->join(
                'inner join',
                '{{%seat_line}} sl',
                's.seat_line_id = sl.id'
            )
            ->where(['in', 's.id', $listSeat]);
        $query2->createCommand();
        $dataSeat = $query2->all();

        $listData2 = [];
        foreach ($dataSeat as $item):
            $listData2[] = $item['list_seat_code'];
        endforeach;
        implode(",", $listData2);

        $listData = [];
        $listData['price_total'] = 0;

        foreach ($data as $item):
            $listData[$item['title']] = $item['qty'];
            $listData['price_total'] += $item['price'];
        endforeach;

        $this->asJson([
            'listData' => $listData,
            'listSeatLine' => $listData2
        ]);
    }
}
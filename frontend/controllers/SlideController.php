<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/1/2018
 * Time: 10:27 AM
 */
namespace frontend\controllers;

use yii\web\Controller;
use common\models\Slide;

class SlideController extends Controller
{
    public function actionIndex(){

    }

    public function actionView($id){
        $model = Slide::findOne(['id' => $id]);
        return $this->render('view',[
            'model' => $model
        ]);
    }
}
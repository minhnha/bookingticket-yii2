<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/1/2018
 * Time: 10:27 AM
 */
namespace frontend\controllers;

use yii\web\Controller;
use common\models\Page;

class PageController extends Controller
{
    public function actionIndex(){

    }

    public function actionView($slug){
        $model = Page::findOne(['slug' => $slug]);
        return $this->render('view',[
            'model' => $model
        ]);
    }
}
<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 8:50 AM
 */
$this->title = 'Danh sách cụm rạp';
?>

<div class="card" style="min-height: 500px">
    <div class="card-header">
        <h4>Danh sách cụm rạp</h4>
    </div>
    <?php foreach ($listCineComplex as $item): ?>
        <div class="card-body h-100">
            <p class="card-text">
                <h5><?= $item->name ?></h5>
                <p><?= $item->address ?></p>
                <p><?= $item->phone_number ?></p>
            </p>
        </div>
    <?php endforeach; ?>
</div>


<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 7:40 AM
 */
use yii\helpers\Url;
$this->title = 'Danh sách phim';
?>
<div class="card">
    <div class="card-header">
        <b>Danh sách phim</b>
    </div>
    <div class="card-deck">
        <?php foreach ($listMovie as $item): ?>
            <div class="col- col-sm-6 col-md-4 col-lg-3 col-xl-3 p-0 mb-1">
                <div class="card">
                    <a class="hover-gallery" href="<?= Url::to(['/movie/view', 'id' => $item->id]) ?>"><img class="card-img-top" src="<?= $item['image'] ?>"
                                                                                                            alt="Card image cap"></a>
                    <div class="card-body">
                        <a href="<?= Url::to(['/movie/view', 'id' => $item->id]) ?>"><h5 class="card-title text-center"><?= $item['title'] ?></h5></a>
                    </div>
                    <div class="card-footer">
                        <small class="text-muted">Last updated: <?= date('d/m/Y h:i A', $item['updated_at']) ?></small>
                    </div>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>

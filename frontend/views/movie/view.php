<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 7:40 AM
 */
$this->title = $model->title;
?>

<div class="card">
    <div class="card-header">
        <b> <?= $model->title ?></b>
    </div>
    <div class="card-body h-100">
        <div class="row">
            <div class="col-md-4">
                <div class="">
                    <img src="<?= $model->image ?>" alt="">
                </div>
            </div>
            <div class="col-md-8">
                <p><b><span style="font-size: 18px" class="mr-3">Diễn viên:</span> <?= $model->cast ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Đạo diễn:</span> <?= $model->director ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Hãng phim:</span> <?= $model->studio ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Thời lượng:</span> <?= $model->length.' phút' ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Định dạng:</span> <?= ($model->version == 0) ? '2D' : '3D' ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Trạng thái:</span> <?php if($model->status==0) echo 'Phim đang chiếu'; else echo 'Phim sắp chiếu'; ?></b></p>
                <p><b><span style="font-size: 18px" class="mr-3">Thể loại:</span> <?= $model->catTitle ?></b></p>
            </div>
        </div>
        <p class="card-text mt-3">
            <?= $model->content ?>
        </p>
    </div>
</div>


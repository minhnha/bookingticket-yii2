<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/16/2018
 * Time: 9:51 AM
 */
use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\datecontrol\DateControl;
use yii\helpers\Url;

$this->title = 'Thông tin tài khoản';

?>

<div class="user-form bg-white p-4">
    <?php $form = ActiveForm::begin([
        'enableAjaxValidation' => true,
    ]); ?>

    <div class="row">
        <div class="col-md-4">
            <?= $form->field($model, 'full_name')->textInput(['readonly' => true])->label('Full Name <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'username')->textInput(['readonly' => true])->label('Username <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Password <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'email')->input('email')->label('Email <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'phone_number')->textInput()->label('Phone Number <span class="required-field">(*)</span>') ?>

        </div>
        <div class="col-md-4">
            <?= $form->field($model, 'gender')->radioList(
                [0 => 'Male', 1 => 'Female']
            ) ?>

            <?=
             $form->field($model, 'birthday')->widget(DateControl::classname(), [
                'readonly' => true,
                'type' => 'date',
                'options' => [],
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        // 'startDate' => Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i'),
                    ]
                ],
                'language' => 'vi'
            ]);
            ?>

            <?= $form->field($model, 'address')->textarea() ?>

        </div>
        <div class="col-md-4">
            <label for="#">Lịch sử đặt (Nhấp để xem chi tiết):</label>
            <?php foreach ($booking as $item): ?>
                <div class="accordion" id="accordionExample">
                    <div class="card">
                        <div class="card-header p-0" id="headingOne">
                            <h5 class="mb-0">
                                <button class="btn btn-link w-100 d-flex" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne" style="text-decoration: none">
                                    <span>Ngày đặt: <?= date('d/m/Y', $item['created_at']) ?></span>
                                    <span class="ml-auto">ID: <?= $item['id'] ?></span>
                                </button>
                            </h5>
                        </div>

                        <div id="collapseOne" class="collapse hide" aria-labelledby="headingOne" data-parent="#accordionExample">
                            <div class="card-body">
                                <p>Họ tên: <span class="full-name"><?= $item['full_name'] ?></span></p>
                                <p>Số điện thoại: <span class="phone-number"><?= $item['phone_number'] ?></span></p>
                                <p>Rạp: <span class="cinema-complex"><?= $item['complex_name'] ?></span></p>
                                <p>Ngày: <span class="date"><?= $item['show_date'] ?></span></p>
                                <p>Phòng chiếu: <span class="screen"><?= $item['screen_name'] ?></span></p>
                                <p>Phim: <span class="movie"></span><?= $item['movie_title'] ?></p>
                                <p>Giờ chiếu: <span class="playtime"><?= $item['show_time'] ?></span></p>
                                <p>Ghế đặt: <span class="list-seat"><?= $item['seat_code'] ?></span></p>
                                <p>Tổng giá vé: <span class="price-total"><?= $item['price'] ?></span></p>
                            </div>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
    <hr>
    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Save', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
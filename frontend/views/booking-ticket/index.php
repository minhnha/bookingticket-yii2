<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 8:50 AM
 */
use yii\bootstrap4\ActiveForm;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\widgets\Pjax;

$this->title = 'Đặt vé online';
?>

<div style="min-height: 500px">
<?php
    if(Yii::$app->user->isGuest){
        echo '
        <div class="d-flex justify-content-center mt-5 flex-column">
            <div class="text-center text-danger mt-2" style="font-size: 20px">Bạn cần đăng nhập mới có thể đặt vé!</div>
            <div class="text-center mt-2">
                '.
                Html::button('Đăng nhập', [
                'value' => Url::to(['site/login']),
                'class' => 'btn-login font-weight-bold bg-transparent modalButtonLogin',
                'style' => 'border:none; cursor:pointer' ])
                .'
            </div>
        </div>
        <div class="d-flex justify-content-center mt-2">Nếu chưa có tài khoản:
            '.
            Html::button('Đăng ký', [
            'value' => Url::to(['site/signup']),
            'class' => 'btn-reg mr-3 font-weight-bold bg-transparent modalButtonRegister',
            'style' => 'border:none; cursor:pointer'
            ])
            .'
        </div>
        ';
?>
<?php
    }else{  ?>
        <div class="card step-1">
            <h5 class="card-header"><?= $this->title ?></h5>
            <div class="card-body">
                <div class="d-flex align-items-center mb-3">
                    <h5 class="card-title mb-0 mr-2">Chọn phim:</h5>
                    <?php Pjax::begin(); ?>
                    <?php $form = ActiveForm::begin([
                        'enableClientValidation' => false,
                        'method' => 'GET',
                        'action' => Url::to(['movie-category/search']),
                        'options' => [
                            'class' => 'form-inline'
                        ]
                    ]) ?>
                    <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search title movie']) ?>
                    <div class="input-group-append">
                        <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                    </div>
                    <?php ActiveForm::end(); ?>
                    <?php Pjax::end(); ?>
                </div>
                <div class="text-center text-white row movie-list">
                    <?php foreach (\common\models\Movie::find()->all() as $item): ?>
                        <div class="col-md-2 mb-2">
                            <div id="<?= $item->id ?>" class="movie-target d-flex align-items-center justify-content-center">
                                <?= $item->title ?>
                            </div>
                        </div>
                    <?php endforeach; ?>
                </div>
                <hr>
                <h5 class="card-title mb-3">Chọn ngày chiếu:</h5>
                <div class="text-center text-white row date-list">
                    <div class="col-md-5"></div>
                    <p class="text-danger">Không có dữ liệu</p>
                </div>
                <hr>
                <h5 class="card-title mb-3">Chọn rạp:</h5>
                <div class="text-center text-white row cine-complex-list">
                    <div class="col-md-5"></div>
                    <p class="text-danger">Không có dữ liệu</p>
                </div>
                <hr>
                <h5 class="card-title mb-3">Chọn phòng chiếu:</h5>
                <div class="text-center text-white row screen-list">
                    <div class="col-md-5"></div>
                    <p class="text-danger">Không có dữ liệu</p>
                </div>
                <hr>
                <h5 class="card-title mb-3">Chọn suất chiếu:</h5>
                <div class="text-center text-white row playtime-list">
                    <div class="col-md-5"></div>
                    <p class="text-danger">Không có dữ liệu</p>
                </div>
                <hr>

                <input type="hidden" name="idShowTimesDetail" id="idShowTimesDetail">

                <button class="btn-next btn btn-dark float-right disabled">Đặt vé <i class="fas fa-arrow-circle-right"></i></button>
            </div>
        </div>
        <!--   End Step-1  -->
        <div class="card step-2 d-none" style="right: -300px">
            <h5 class="card-header">Thông tin đặt vé</h5>
            <div class="card-body d-flex justify-content-center align-items-center" style="height: 600px; background: #f5f5f5">
                <div class="bg-white p-4 booking-info" style="width: 500px">
                    <h3 class="text-center">THÔNG TIN ĐẶT VÉ</h3>
                    <p>Họ tên: <span class="full-name"><?= Yii::$app->user->identity->full_name ?></span></p>
                    <p>Số điện thoại: <span class="phone-number"><?= Yii::$app->user->identity->phone_number ?></span></p>
                    <p>Rạp: <span class="cinema-complex"></span></p>
                    <p>Ngày: <span class="date"></span></p>
                    <p>Phòng chiếu: <span class="screen"></span></p>
                    <p>Phim: <span class="movie"></span></p>
                    <p>Giờ chiếu: <span class="playtime"></span></p>
                    <p>Ghế đặt: <span class="list-seat"></span></p>
                    <p>Tổng giá vé: <span class="price-total"></span></p>
                    <?php
                    $form = ActiveForm::begin([
                        'action' => Url::to(['booking-ticket/create']),
                        'enableClientValidation' => false
                    ]);
                    ?>
                    <?= $form->field($model, 'full_name')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'phone_number')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'complex_name')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'screen_name')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'movie_title')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'show_date')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'show_time')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'seat_code')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'normal_ticket')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'vip_ticket')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'price')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'description')->textarea()->label('Ghi chú:') ?>
                    <?= $form->field($model, 'show_times_detail_id')->hiddenInput()->label(false) ?>
                    <?= $form->field($model, 'pass_flag')->hiddenInput(['value' => true])->label(false) ?>
                    <input type="hidden" name="seatChecked" id="seatChecked">
                    <div class="d-flex justify-content-center">
                        <button class="btn-prev btn btn-info mr-2">Quay lại</button>
                        <button type="submit" class="btn-accept btn btn-success">Xác nhận</button>
                    </div>
                    <?php
                    ActiveForm::end();
                    ?>
                </div>
            </div>
        </div>
        <!--  End step-2  -->
    <?php
    $urlMovie = Url::to(['booking-ticket/get-show-times-by-movie']);
    $urlGetListSeat = Url::to(['booking-ticket/get-list-seat-by-show-times-detail']);
    $fullName = Yii::$app->user->identity->full_name;
    $phoneNumber = Yii::$app->user->identity->phone_number;
    $this->registerJs(<<<JS
    $('.movie-target').click(function () {
        $('.movie-target').removeClass('checked');
        $(this).toggleClass('checked');
        // Event change on movie
   
        let movie_id = $(this).attr('id');
        let data = {
            movie_id: movie_id
        }
        $.ajax({ 
            url: '$urlMovie',
            method: 'GET',
            data: data,
            success: function(data) {
                $('.date-list').html(data['listDate']);
                $('.date-target').append(data['script']);
                $('.btn-next').addClass('disabled');
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });

    
    $('.btn-next').click(function() {
        if($('.btn-next').hasClass('disabled')){
            alert('Vui lòng chọn đầy đủ các thông tin trên!');
            return false;
        }
        
        let hClass = $('.card-body div').hasClass('checked');
        if(hClass){
            let idShowTimeDetail = $('#idShowTimesDetail').val();
            let data = { idShowTimeDetail:idShowTimeDetail };
            $.ajax({ 
                url: '$urlGetListSeat',
                method: 'GET',
                data: data,
                success: function(data) {
                    $('#seat-control').html(data);
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
            $('#chooseSeat').modal('show');
        }
    });
    
    $('.btn-prev').click(function() {
        $('.step-2').addClass('d-none');
        $('.step-1').removeClass('d-none').animate({left: '0px'}, 1000);
    });
    
    $('#booking-full_name').val('$fullName');
    $('#booking-phone_number').val('$phoneNumber');

JS
            , $this::POS_LOAD);
        ?>
    <?php } ?>
</div>
<!--End page booking ticket-->

<!-- Modal Choose Seat -->
<div class="modal fade" id="chooseSeat" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body position-relative">
                <div class="border text-white text-center" style="background: #8D1B5B; font-size: 20px; margin-bottom: 80px">
                    Màn hình
                </div>
                <div class="note-status-seat position-absolute d-flex align-items-center">
                    <span class="bg-represent" style="background: #FF5900"></span> <span class="ml-1">Hàng ghế</span>
                    <span class="bg-represent ml-3" style="background: #f5f5f5"></span> <span class="ml-1">Ghế đã đặt</span>
                    <span class="bg-represent ml-3" style="background: #B24D80"></span> <span class="ml-1">Ghế thường</span>
                    <span class="bg-represent ml-3" style="background: #e4b301"></span> <span class="ml-1">Ghế vip</span>
                </div>
                <div id="seat-control">
                    <p class="text-center text-danger">Không có dữ liệu</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary btn-SeatChecked">Save changes</button>
            </div>
        </div>
    </div>
</div>
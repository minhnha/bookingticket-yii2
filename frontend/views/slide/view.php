<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/1/2018
 * Time: 11:11 AM
 */

?>

<div class="card">
    <div class="card-header">
        <b> <?= $model->title ?></b>
    </div>
    <div class="card-body h-100">
        <p class="card-text">
            <?= $model->content ?>
        </p>
    </div>
</div>

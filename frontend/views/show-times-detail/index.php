<?php
/**
 * Created by PhpStorm.
 * User: Phamt
 * Date: 12/6/2018
 * Time: 8:54 AM
 */

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;

/* @var $this yii\web\View */
/* @var $searchModel frontend\models\ShowTimesDetailSearch */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Lịch chiếu phim';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="card">
    <div class="card-header">
        <h4><?= Html::encode($this->title) ?></h4>
    </div>
    <div class="card-body pb-5">
        <div class="show-times-detail-index inner-detail">
            <?php // echo $this->render('_search', ['model' => $searchModel]); ?>
            <?php Pjax::begin(); ?>
                <?= GridView::widget([
                    'dataProvider' => $dataProvider,
                    'filterModel' => $searchModel,
                    'tableOptions' => ['class' => 'table table-bordered table-hover table-responsive-md table-dark position-relative'],
                    'columns' => [
                        ['class' => 'yii\grid\SerialColumn'],

                        //'id',
                        [
                            'attribute' => 'show_times_id',
                            'value' => 'showTimesDate'
                        ],
                        [
                            'attribute' => 'cine_complex_id',
                            'value' => 'cineComplex.name'
                        ],
                        [
                            'attribute' => 'screen_id',
                            'value' => 'screen.name'
                        ],
                        [
                            'attribute' => 'movie_id',
                            'value' => 'movie.title'
                        ],
                        [
                            'attribute' => 'playtime_id',
                            'value' => 'playTimeDetail'
                        ],
                        'normal_seat_remaining',
                        'vip_seat_remaining',
                        // 'created_at',
                        // 'created_by',
                        // 'updated_at',
                        // 'updated_by',

                        //['class' => 'yii\grid\ActionColumn'],
                    ],
                    'pager' => [
                        'class' => 'justinvoelker\separatedpager\LinkPager',
                        'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                        'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                        'maxButtonCount' => 7,
                        'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
                    ]
                ]); ?>
            <?php Pjax::end(); ?>
        </div>
    </div>
</div>

<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

$this->title = 'Signup';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="site-signup">
        <div class="col-lg-12">
            <?php $form = ActiveForm::begin([
                'id' => 'form-signup',
                'enableAjaxValidation' => true
            ]); ?>
                <?= $form->field($model, 'full_name')->textInput() ?>

                <?= $form->field($model, 'username')->textInput(['autofocus' => true]) ?>

                <?= $form->field($model, 'password')->passwordInput() ?>

                <?= $form->field($model, 'email')->input('email') ?>

                <?= $form->field($model, 'phone_number')->textInput() ?>

                <div class="form-group">
                    <?= Html::submitButton('Đăng ký', ['class' => 'btn btn-primary btn-submit', 'name' => 'signup-button']) ?>
                </div>

            <?php ActiveForm::end(); ?>
        </div>
</div>

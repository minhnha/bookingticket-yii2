<?php
use yii\helpers\Html;
use yii\helpers\Url;
/* @var $this yii\web\View */

$this->title = 'Trang chủ';
?>

<div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
    <div class="carousel-inner">
        <?php $active = 'active';
        foreach ($listSlide as $item):
            ?>
            <div class="carousel-item <?= $active; $active=''; ?>">
                <a href="<?= Url::to(['/slide/view', 'id' => $item->id]) ?>">
                    <img class="d-block w-100" src="<?= $item->link_image ?>" alt="<?= $item->title ?>">
                </a>
            </div>
        <?php endforeach; ?>
    </div>
    <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
        <span class="sr-only">Previous</span>
    </a>
    <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
        <span class="carousel-control-next-icon" aria-hidden="true"></span>
        <span class="sr-only">Next</span>
    </a>
</div>
<!-- End Slider -->

<ul class="nav nav-tabs tab-movie-status" id="myTab" role="tablist">
    <li class="nav-item">
        <a class="nav-link active" id="home-tab" data-toggle="tab" href="#movie_show" role="tab"
           aria-controls="movie_show" aria-selected="true">Phim đang chiếu</a>
    </li>
    <li class="nav-item">
        <a class="nav-link" id="profile-tab" data-toggle="tab" href="#movie_coming" role="tab"
           aria-controls="movie_coming" aria-selected="false">Phim sắp chiếu</a>
    </li>
</ul>
<div class="tab-content" id="myTabContent">
    <div class="tab-pane fade show active" id="movie_show" role="tabpanel" aria-labelledby="home-tab">
        <div class="card-deck">
            <?php foreach ($listMovieShow as $item){ ?>
                <div class="col- col-sm-6 col-md-4 col-lg-3 col-xl-3 p-0 mb-1">
                    <div class="card">
                        <a class="hover-gallery" href="<?= Url::to(['/movie/view', 'id' => $item->id]) ?>"><img class="card-img-top" src="<?= $item['image'] ?>"
                                         alt="Card image cap"></a>
                        <div class="card-body">
                            <?= Html::a('<h5 class="card-title text-center">' .$item['title']. '</h5>',['movie/view', 'id' => $item->id]) ?>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Last updated: <?= date('d/m/Y h:i A', $item['updated_at']) ?></small>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- end card-deck -->
    </div>
    <!-- End tab movie-show -->

    <div class="tab-pane fade" id="movie_coming" role="tabpanel" aria-labelledby="profile-tab">
        <div class="card-deck">
            <?php foreach ($listMovieComing as $item){ ?>
                <div class="col- col-sm-6 col-md-4 col-lg-3 col-xl-3 p-0 mb-1">
                    <div class="card">
                        <a class="hover-gallery" href="<?= Url::to(['/movie/view', 'id' => $item->id]) ?>"><img class="card-img-top" src="<?= $item['image'] ?>"
                                         alt="Card image cap"></a>
                        <div class="card-body">
                            <a href="#"><h5 class="card-title text-center"><?= $item['title'] ?></h5></a>
                        </div>
                        <div class="card-footer">
                            <small class="text-muted">Last updated: <?= date('d/m/Y h:i A', $item['updated_at']) ?></small>
                        </div>
                    </div>
                </div>
            <?php } ?>
        </div>
        <!-- end card-deck -->
    </div>
    <!-- End tab-movie-coming -->
</div>
<!-- End tab-content -->
<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use frontend\assets\AppAsset;
use common\widgets\Alert;
use yii\bootstrap4\Modal;
use yii\helpers\Url;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
    <div class="container p-0">
        <div class="container bg-header p-3">
            <div class="row">
                <div class="col-md-3">
                    <?= Html::a('<img src="'.Url::to(['img/logo.png']).'" title="Logo {BE}" alt="Logo {BE}">', ['/']) ?>
                </div>
                <div class="col-md-9 d-flex flex-row-reverse btn-action">
                    <?php if(Yii::$app->user->isGuest): ?>
                        <!-- Button đăng nhập  -->
                        <?= Html::button('Đăng nhập', [
                            'value' => Url::to(['site/login']),
                            'class' => 'btn-login font-weight-bold bg-transparent modalButtonLogin',
                            'style' => 'border:none; cursor:pointer'
                        ]) ?>
                        <!--  Button đăng ký   -->
                        <?= Html::button('Đăng ký', [
                            'value' => Url::to(['site/signup']),
                            'class' => 'btn-reg mr-3 font-weight-bold bg-transparent modalButtonRegister',
                            'style' => 'border:none; cursor:pointer'
                        ]) ?>
                    <?php else: ?>
                        <?= Html::a('Đăng xuất', ['site/logout'], ['data-method' => 'post', 'class' => 'btn-action btn-logout']) ?>
                        <?= Html::a('('.Yii::$app->user->identity->full_name.')', ['/user/index', 'id' => Yii::$app->user->identity->id], ['class' => 'btn-action font-weight-bold bg-transparent mr-3']) ?>
                        <span class="mr-1">Xin chào</span>
                    <?php endif; ?>

                    <?php Modal::begin([
                        'title' => 'Đăng ký',
                        'id' => 'modal-register',
                        'size' => 'modal-sm'
                    ]); ?>
                    <?= '<div id="modalRegisterContent"></div>' ?>
                    <?php Modal::end(); ?>

                    <?php Modal::begin([
                        'title' => 'Đăng nhập',
                        'id' => 'modal-login',
                        'size' => 'modal-sm'
                    ]); ?>
                    <?= '<div id="modalLoginContent"></div>' ?>
                    <?php Modal::end(); ?>
                </div>
            </div>
        </div>

        <nav class="navbar navbar-expand-lg bg-main-menu">
            <a class="navbar-brand" href="<?= Url::to(['/site/index']) ?>">Trang chủ</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto main-menu">
                    <li class="nav-item active">
                        <a class="nav-link" href="<?= Url::to(['/show-times-detail/index']) ?>">Lịch chiếu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::to(['/movie/index']) ?>">Phim</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::to(['/cinema-complex/index']) ?>">Rạp chiếu phim</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::to(['/page/view', 'slug' => 'lien-he']) ?>">Liên hệ</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::to(['/page/view', 'slug' => 'gioi-thieu']) ?>">Giới thiệu</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" href="<?= Url::to(['/booking-ticket']) ?>">Đặt vé online</a>
                    </li>
                </ul>
            </div>
        </nav>

        <?= Alert::widget() ?>

        <?= $content ?>

        <table width="100%" height="150px" class="footer">
            <tr>
                <td rowspan="2">
                    <p class="thongtin">
                        <strong>{BE} Cinema</strong><br/>
                        <strong>Địa chỉ:</strong> Đông Xuyên, TP.Long Xuyên, An Giang<br/>
                        <strong>Điện thoại: </strong>(029) 3 333333 - (029) 3 999999<br/>
                        <strong>Email:</strong> <a href="mailto:be-cinema@gmail.com"> be-cinema@gmail.com</a><br/>
                    </p>
                </td>
                <td></td>
            </tr>
            <tr>
                <td></td>
                <td>
                    <div class="LienKetWeb">
                        <a href="https://twitter.com/?lang=vi"><i class="fab fa-twitter-square"></i></a>
                        <a href="https://www.linkedin.com/"><i class="fab fa-linkedin-in"></i></a>
                        <a href="https://www.instagram.com/"><i class="fab fa-instagram"></i></a>
                        <a href="https://facebook.com"><i class="fab fa-facebook-square"></i></a>
                    </div>
                </td>
            </tr>
        </table>
    </div>


<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>

<?php

namespace frontend\assets;
use Yii;
use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        //'css/bootstrap.min.css',
        'css/fonts/font-awesome/css/font-awesome.5.1.min.css',
        //'css/font-awesome.min.css',
        'css/style.css',
    ];
    public $js = [
        //'js/popper.min.js',
        //'js/bootstrap.min.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',

    ];
}

<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SeatCategory */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Seat Categories', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seat-category-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'price',
            'discount',
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

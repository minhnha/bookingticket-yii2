<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\SeatCategory */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seat-category-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->isNewRecord ? 'create-form' : 'update-form',
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'price')->textInput() ?>

    <?= $form->field($model, 'discount')->input('number',['min' => 0, 'max' => 100, 'value' => 0]) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

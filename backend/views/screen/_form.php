<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Screen */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="screen-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->isNewRecord ? 'create-form' : 'update-form',
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'normal_seat_quantity')->input('number', ['maxlength' => true, 'min' => 0]) ?>

    <?= $form->field($model, 'vip_seat_quantity')->input('number' ,['maxlength' => true, 'min' => 0]) ?>

    <?= $form->field($model, 'cine_complex_id')->dropDownList($model->getListCineComplex(),[
        'prompt' => '-- Choose cinema complex --'
    ]) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

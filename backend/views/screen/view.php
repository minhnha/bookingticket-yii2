<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Screen */

$this->title = $model->name;
$this->params['breadcrumbs'][] = ['label' => 'Screens', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="screen-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'name',
            'normal_seat_quantity',
            'vip_seat_quantity',
            [
                'attribute' => 'nameCineComplex',
                'label' => 'Cinema complex'
            ],
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

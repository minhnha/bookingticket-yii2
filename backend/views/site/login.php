<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \common\models\LoginForm */

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;
use common\widgets\Alert;

$this->title = 'Login';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="container-fluid bg-login">
    <div class="row mr-none">
        <div class="col-md-6 wp-login d-flex justify-content-center">
            <?php $form = ActiveForm::begin(
                ['options' =>
                    ['class' => 'login-form']
                ]
            ); ?>
            <?= Alert::widget() ?>
            <img src="<?php echo Url::to(['images/logo.png']) ?>" alt="" class="logo">
            <hr class="float-left">
            <div class="form-group mr-b-username">
                <?= $form->field($model, 'username')->textInput(['autofocus' => true, 'class' => 'form-control txt-username txt-input', 'placeholder'=>'Username'])->label(false) ?>
            </div>
            <div class="form-group mr-b-password">
                <?= $form->field($model, 'password')->passwordInput(['class' => 'form-control txt-password', 'placeholder'=>'Password'])->label(false) ?>
            </div>

            <div class="form-group">
                <?= $form->field($model, 'rememberMe')->checkbox(['class' => 'form-check-input cb-remember', 'id' =>'remember-user'])->label(false) ?>
                <label class="form-check-label pl-4" for="remember-user">Remember me</label>
            </div>

            <?= Html::submitButton('Login', ['class' => 'btn btn-primary btn-login', 'name' => 'login-button']) ?>
            <?php ActiveForm::end(); ?>
        </div>
        <div class="col-md-6 login-img">

        </div>
    </div>
</div>

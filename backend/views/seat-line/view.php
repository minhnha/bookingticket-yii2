<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\SeatLine */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seat Lines', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seat-line-view">
    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'line',
            'seat_quantity',
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>
</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Playtime */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Playtimes', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="playtime-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            [
                'attribute' => 'start_time',
                'value' => function($model){
                    return date('H:i', $model->start_time);
                }
            ],
            [
                'attribute' => 'end_time',
                'value' => function($model){
                    return date('H:i', $model->end_time);
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

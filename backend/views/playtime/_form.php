<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model common\models\Playtime */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="playtime-form">
    <?php $action = $model->isNewRecord ? 'create-form' : 'update-form' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'start_time')->widget(DateControl::classname(), [
        'type' => 'time',
        'readonly' => true,
        'options' => ['id' => $action.'-dtp-start-time'],
        'widgetOptions' => [
            'pluginOptions' => [
                'autoclose' => true,
            ]
        ],
        'language' => 'vi'
    ]);
    ?>

    <?= $form->field($model, 'end_time')->widget(DateControl::classname(), [
        'type' => 'time',
        'readonly' => true,
        'options' => ['id' => $action.'-dtp-end-time'],
        'widgetOptions' => [
            'pluginOptions' => [
                'autoclose' => true,
                //'startDate' => Yii::$app->formatter->asDatetime('now', 'php:d/m/Y'),
            ]
        ],
        'language' => 'vi'
    ]);
    ?>
    <hr class="mt-5">
    <div class="form-group d-flex justify-content-end mb-1 mt-1">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$this->registerJs(<<<JS
    $('#create-form-dtp-start-time-disp').val(null);
    $('#create-form-dtp-end-time-disp').val(null);
    $('.invalid-feedback').addClass('d-block');
JS
    , $this::POS_READY);
?>

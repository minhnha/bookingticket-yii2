<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\ShowTimesDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="show-times-detail-form">

    <?php $action = $model->isNewRecord ? 'create-form' : 'update-form' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true,
    ]); ?>

    <?php
        if($action == 'update-form')
        {
            echo $form->field($model, 'id')->hiddenInput()->label(false);
        }
    ?>

    <?= $form->field($model, 'show_times_id')->dropDownList(
        $model->getListShowTimes(), [
            'prompt' => '--- Choose Date ---'
        ]
    ) ?>

    <?= $form->field($model, 'cine_complex_id')->dropDownList(
        $model->getListCineComplex(), [
            'prompt' => '--- Choose CineComplex ---'
        ]
    ) ?>

    <?= $form->field($model, 'screen_id')->dropDownList([
            'prompt' => '--- Choose Screen ---'
        ]
    ) ?>

    <?= $form->field($model, 'movie_id')->dropDownList(
        $model->getListMovie(), [
            'prompt' => '--- Choose Movie ---'
        ]
    ) ?>

    <?= $form->field($model, 'playtime_id')->dropDownList(
        $model->getListPlaytime(),[
            'prompt' => '--- Choose Playtime ---'
        ]
    ) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$urlCineComplex = \yii\helpers\Url::to(['show-times-detail/search-by-cinema-complex']);
$this->registerJs(<<<JS
    $('#showtimesdetail-cine_complex_id').change(function() {
        let complex_id = $(this).val();
            let data = { complex_id: complex_id }
            $.ajax({ 
                url: '$urlCineComplex',
                method: 'GET',
                data: data,
                success: function(data) {
                    $('#showtimesdetail-screen_id').html(data);
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
    });
JS
    , $this::POS_READY);

if ($action == 'update-form'){
    $urlGetScreenId = \yii\helpers\Url::to(['show-times-detail/get-screen-by-id']);
    $this->registerJs(<<<JS
        let complex_id = $('#showtimesdetail-cine_complex_id').val();
        let data = { complex_id: complex_id }
        $.ajax({ 
            url: '$urlCineComplex',
            method: 'GET',
            data: data,
            success: function(data) {
                $('#showtimesdetail-screen_id').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
        
        let id = $('#showtimesdetail-id').val();
        let res = { id: id }
        $.ajax({ 
            url: '$urlGetScreenId',
            method: 'GET',
            data: res,
            success: function(res) {
                $('#showtimesdetail-screen_id').val(res);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
        
JS
        , $this::POS_READY);
}
?>

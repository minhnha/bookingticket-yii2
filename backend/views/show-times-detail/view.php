<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\ShowTimesDetail */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Show Times Details', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="show-times-detail-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'cineComplex.name',
            'showTimesDate',
            'screen.name',
            'movie.title',
            'playTimeDetail',
            'normal_seat_remaining',
            'vip_seat_remaining',
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */
/* @var $form yii\widgets\ActiveForm */

?>

<div class="booking-form">
    <?php $action = $model->isNewRecord ? 'create' : 'update' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'full_name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <?php
        if($action == 'create')
        {
            echo $form->field($model, 'complex_name')->dropDownList(
                $showTimes->getListCineComplex(),
                [
                    'prompt' => '--- Choose CinemaComplex ---'
                ]);
            echo $form->field($model, 'show_date')->dropDownList([
                'prompt' => '--- Choose Date ---'
            ]);
            echo $form->field($model, 'screen_name')->dropDownList([
                'prompt' => '--- Choose Screen ---',

            ]);
            echo $form->field($model, 'movie_title')->dropDownList([
                'prompt' => '--- Choose Movie ---'
            ]);
            echo $form->field($model, 'show_time')->dropDownList([
                'prompt' => '--- Choose Time ---'
            ]);
        }
        else
        {
            echo $form->field($model, 'show_times_detail_id')->hiddenInput()->label(false);

            //$model->complex_name = \common\models\CinemaComplex::findOne(['name' => $model->complex_name])->id;
            echo  $form->field($model, 'complex_name')->dropDownList(
                $showTimes->getListCineComplex()
            );

            //$model->show_date = \common\models\ShowTimes::findOne(['date'=> $model->show_date])->id;
            echo $form->field($model, 'show_date')->dropDownList(
                $showTimes->getListShowTimes()
            );

            //$model->screen_name = \common\models\Screen::findOne(['name' => $model->screen_name])->id;
            echo $form->field($model, 'screen_name')->dropDownList(
                $showTimes->getListScreen()
            );

            //$model->movie_title = \common\models\Movie::findOne(['title' => $model->movie_title])->id;
            echo $form->field($model, 'movie_title')->dropDownList(
                $showTimes->getListMovie()
            );

            echo $form->field($model, 'show_time')->dropDownList(
                $showTimes->getListPlaytime()
            );
        }
    ?>


    <?= $form->field($model, 'seat_code',['options' => ['class' => 'mb-1']])->textInput(['readonly'=>true]) ?>
    <!-- Button trigger modal choose seat -->
    <button type="button" class="btn btn-primary btn-xs mb-2 btn-choose-seat" data-toggle="modal" data-target="#chooseSeat" <?php if($action=='update') echo 'disabled'; ?>>
        Choose Seats
    </button>
    <button type="button" id="btn-delete-seat" class="btn btn-danger btn-xs mb-2 btn-delete-seat" <?php if($action=='create') echo 'disabled'; ?>>
        Clean Seats
    </button>

    <?= $form->field($model, 'price')->input('number',['readonly' => true]) ?>

    <?= $form->field($model, 'normal_ticket')->hiddenInput(['readonly' => true])->label(false) ?>

    <?= $form->field($model, 'vip_ticket')->hiddenInput(['readonly' => true])->label(false) ?>

<!--    echo $form->field($model, 'status')->radioList([0 => 'Unpaid', 1 => 'Paid']) -->

    <?= $form->field($model, 'description')->textarea(['maxlength' => true]) ?>

    <input type="hidden" id="seatChecked" name="seatChecked">
    <input type="hidden" id="showTimesId" name="showTimesId">

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary' , 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php
$urlCineComplex = Url::to(['booking/search-by-cinema-complex']);
$urlShowTimes = Url::to(['booking/search-by-show-times']);
$urlScreen = Url::to(['booking/search-by-screen']);
$urlMovie = Url::to(['booking/search-by-movie']);
$urlGetPrice = Url::to(['booking/price-by-seat-id']);
$urlGetIdShowTimesDetail = Url::to(['booking/get-id-show-times-detail']);
$urlGetDataUpdate = Url::to(['booking/get-data-update']);
$this->registerJs(<<<JS
    // Clean data screen when open form
    $('#seat-control').html('<p class="text-center text-danger">Không có dữ liệu</p>');
    $('#btn-delete-seat').click(function() {
        $('#booking-seat_code').val(null);
        $('#booking-price').val(null);
        $('.btn-choose-seat').removeAttr('disabled');
        $(this).attr('disabled', true);
       
        let hClass = $('ul.sub-list li').hasClass('checked');
        if (hClass){
            $('ul.sub-list li').removeClass('checked');
        } 
    });

    // Event change on complex_name
    $('#booking-complex_name').change(function() {
        let complex_id = $(this).val();
        let data = { complex_id: complex_id }
        $.ajax({ 
            url: '$urlCineComplex',
            method: 'GET',
            data: data,
            success: function(data) {
                $('#booking-show_date').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    // Event change on show date
    $('#booking-show_date').change(function() {
        let show_times_id = $(this).val();
        let complex_id = $('#booking-complex_name').val();
        let data = { show_times_id: show_times_id, complex_id: complex_id }
        $.ajax({
            url: '$urlShowTimes',
            method: 'GET',
            data: data,
            success: function(data) {
                $('#booking-screen_name').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    // Event change on screen
    $('#booking-screen_name').change(function() {
        let screen_id = $(this).val();
        let complex_id = $('#booking-complex_name').val();
        let show_times_id = $('#booking-show_date').val();
        let data = { 
            show_times_id: show_times_id, 
            complex_id: complex_id, 
            screen_id: screen_id 
        }
        $.ajax({ 
            url: '$urlScreen',
            method: 'GET',
            data: data,
            success: function(data) {
                $('#booking-movie_title').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    // Event change on movie
    $('#booking-movie_title').change(function() {
        let movie_id = $(this).val();
        let complex_id = $('#booking-complex_name').val();
        let show_times_id = $('#booking-show_date').val();
        let screen_id = $('#booking-screen_name').val();
        let data = { 
            show_times_id: show_times_id, 
            complex_id: complex_id, 
            screen_id: screen_id,
            movie_id: movie_id
        }
        $.ajax({ 
            url: '$urlMovie',
            method: 'GET',
            data: data,
            success: function(data) {
                $('#booking-show_time').html(data);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    
    // Event change show time
    $('#booking-show_time').change(function() {
        let playtime_id = $(this).val();
        let complex_id = $('#booking-complex_name').val();
        let show_times_id = $('#booking-show_date').val();
        let screen_id = $('#booking-screen_name').val();
        let movie_id =  $('#booking-movie_title').val();
        let data = { 
            show_times_id: show_times_id, 
            complex_id: complex_id, 
            screen_id: screen_id,
            movie_id: movie_id,
            playtime_id: playtime_id
        }
        $.ajax({ 
            url: '$urlGetIdShowTimesDetail',
            method: 'GET',
            data: data,
            success: function(data) {
                // Load data seat in screen to modal
                $('#seat-control').html(data);
                
                let id = $('#getShowTimesId').val();
                $('#showTimesId').val(id);
            },
            error: function(xhr, ajaxOptions, thrownError){
                alert(xhr.status);
                alert(thrownError);
            }
        });
    });
    
    // Checked and choose Seat
    $('#chooseSeat').on('hide.bs.modal', function() {
        let hClass = $('ul.sub-list li').hasClass('checked');
        if(hClass){
            let listSeat = $('#booking-seat_code').val();
            let data = { listSeat: listSeat }
            $.ajax({ 
                url: '$urlGetPrice',
                method: 'GET',
                data: data,
                success: function(data) {
                    $('#booking-seat_code').val(data['listSeatLine']);
                    $('#booking-price').val(data['listData']['price_total']);
                    $('#booking-normal_ticket').val(data['listData']['Normal']);
                    $('#booking-vip_ticket').val(data['listData']['Vip']);
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
        }
    })
JS
    , $this::POS_READY);
if($action == 'update'){
    $this->registerJs(<<<JS
        getDateUpdate();
        $('.modalButtonUpdate').click(function() {
            getDateUpdate();
        });
        function getDateUpdate() {
            let show_times_detail_id = $('#booking-show_times_detail_id').val();
            let data = {
                show_times_detail_id: show_times_detail_id
            }
            $.ajax({ 
                url: '$urlGetDataUpdate',
                method: 'GET',
                data: data,
                success: function(data) {
                    $('#booking-complex_name').val(data['dataUpdate'].cine_complex_id);
                    $('#booking-show_date').val(data['dataUpdate'].show_times_id);
                    $('#booking-screen_name').val(data['dataUpdate'].screen_id);
                    $('#booking-movie_title').val(data['dataUpdate'].movie_id);
                    $('#booking-show_time').val(data['dataUpdate'].playtime_id);
                },
                error: function(xhr, ajaxOptions, thrownError){
                    alert(xhr.status);
                    alert(thrownError);
                }
            });
    
            $("#booking-show_time").trigger("change");
            $('#btn-delete-seat').click(function() {
                $('#booking-seat_code').val(null);
                $('#booking-price').val(null);
                $('.btn-choose-seat').removeAttr('disabled');
                $(this).attr('disabled', true);
               
                let hClass = $('ul.sub-list li').hasClass('checked');
                if (hClass){
                    $('ul.sub-list li').removeClass('checked');
                } 
            });
        }
     
JS
        , $this::POS_READY);
}
?>

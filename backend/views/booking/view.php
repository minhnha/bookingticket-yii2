<?php

use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Booking */

//$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Bookings', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="booking-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'full_name',
            'phone_number',
            'seat_code',
            'complex_name',
            'screen_name',
            'movie_title',
            'show_date',
            'show_time',
            'normal_ticket',
            'vip_ticket',
            [
                'attribute' => 'price',
                'value' => function($model){
                    return $model->formatNumber($model->price, null);
                }
            ],
            'description',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if ($model->status == 1)
                        return 'Paid';
                    return 'Unpaid';
                }
            ],
            //'show_times_detail_id',
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
        ],
    ]) ?>

</div>

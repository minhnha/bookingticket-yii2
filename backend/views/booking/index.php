<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Bookings';
$this->params['breadcrumbs'][] = $this->title;

?>
<div class="inner-head">
    <?= Html::encode($this->title) ?>
</div>
<div class="inner-content">
    <div class="inner-top d-flex mb-3">
        <span class="title align-items-center">
            <img src="<?= Url::to(['images/bullet-room-list.png']) ?>" alt="bullet-booking-list"
                 title="Booking List">Booking List
        </span>
        <div class="filter-data mt-1 ml-3">
            <div class="input-group search-data">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'method' => 'GET',
                    'action' => Url::to(['booking/search']),
                    'options' => [
                        'class' => 'form-inline'
                    ]
                ]) ?>
                <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search']) ?>
                <div class="input-group-append">
                    <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="btn-create ml-auto">
            <?= Html::button('<i class="fas fa-plus-circle"></i> Create', ['value' => Url::To(['booking/create']),'class' => 'btn btn-primary modalButtonCreate']) ?>
            <?php
            Modal::begin([
                'title' => '<h5>Create New Booking</h5>',
                'id' => 'modal-create',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalCreateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Update Booking</h5>',
                'id' => 'modal-update',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalUpdateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Detail Booking</h5>',
                'id' => 'modal-view',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalViewContent'></div>"
            ?>
            <?php Modal::end(); ?>
        </div>
    </div>
    <div class="inner-detail table-responsive-md">
        <?php Pjax::begin(); ?>
        <?= GridView::widget([
            'tableOptions' => [
                'class' => 'table table-bordered table-hover table-responsive-md'
            ],
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn'],

                //'id',
                'full_name',
                'phone_number',
                'seat_code',
                'complex_name',
                //'screen_name',
                //'movie_title',
                // 'show_date',
                // 'show_time',
                // 'normal_ticket',
                // 'vip_ticket',
                [
                    'attribute' => 'price',
                    'value' => function($model){
                        return $model->formatNumber($model->price, null);
                    }
                ],
                // 'description',
                [
                    'attribute' => 'status',
                    'content' => function($model){
                        if ($model->status == 1)
                            return '<span class="badge badge-pill badge-success">Paid</span>';
                        return '<span class="badge badge-pill badge-danger">Unpaid</span>';
                    }
                ],
                // 'show_times_id:datetime',
                // 'user_id',
                // 'created_at',
                // 'updated_at',

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'headerOptions' => [
                        'class' => 'text-center'
                    ],
                    'contentOptions' => [
                        'class' => 'text-center'
                    ],
                    'buttons' => [
                        'view' => function($url){
                            return Html::button('<i class="fas fa-eye"></i> View',[
                                'class' => 'btn btn-success btn-xs modalButtonView',
                                'value' => $url,
                            ]);
                        },
                        'update' => function($url){
                            return Html::button('<i class="fas fa-edit"></i> Edit',[
                                'value' => $url,
                                'class' => 'btn btn-primary btn-xs modalButtonUpdate',
                            ]);
                        },
                        'delete' => function($url, $model){
                            return Html::a('<i class="fas fa-trash-alt"></i> Delete', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'data-confirm' => 'Bạn có muốn xóa bảng ghi đặt vé của '.$model->full_name.' không?',
                                'data-method' => 'POST'
                            ]);
                        }
                    ]
                ],
            ],
            'pager' => [
                'class' => 'justinvoelker\separatedpager\LinkPager',
                'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                'maxButtonCount' => 7,
                'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
            ]
        ]); ?>
        <?php Pjax::end(); ?>
    </div>

    <!-- Modal Choose Seat -->
    <div class="modal fade" id="chooseSeat" tabindex="-1" role="dialog">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body position-relative">
                    <div class="border text-white text-center" style="background: #8D1B5B; font-size: 20px; margin-bottom: 80px">
                        Màn hình
                    </div>
                    <div class="note-status-seat position-absolute d-flex align-items-center">
                        <span class="bg-represent" style="background: #FF5900"></span> <span class="ml-1">Seat line</span>
                        <span class="bg-represent ml-3" style="background: #f5f5f5"></span> <span class="ml-1">Seat booked</span>
                        <span class="bg-represent ml-3" style="background: #B24D80"></span> <span class="ml-1">Seat normal</span>
                        <span class="bg-represent ml-3" style="background: #e4b301"></span> <span class="ml-1">Seat vip</span>
                    </div>
                    <div id="seat-control">
                        <p class="text-center text-danger">Không có dữ liệu</p>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary btn-SeatChecked">Save changes</button>
                </div>
            </div>
        </div>
    </div>
</div>

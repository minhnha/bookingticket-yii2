<?php

/* @var $this \yii\web\View */
/* @var $content string */
use yii\helpers\Url;

if(Yii::$app->user->isGuest){
    die('You not access this page when you are not logged in!!  <a href="'.Url::to(['site/login']).'">Click to login</a>');
}

use backend\assets\AppAsset;
use yii\helpers\Html;
use common\widgets\Alert;
use yii\bootstrap4\Modal;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="container-fluid bg-container">
    <div class="row">
        <div class="col-md-2half sidebar">
            <div class="logo-manage d-flex justify-content-center">
                <a href="<?= Url::to(['site/index']) ?>"><img src="<?= Url::to(['images/room-manage-logo.png']) ?>" alt="Logo BE" title="Logo BE" class="img-fluid"></a>
            </div>
            <!-- End logo -->
            <div class="user">
                <div class="content-user">
                    <a href="#" id="<?= Yii::$app->user->identity->id ?>" class="float-left"><img src="<?php echo Url::to(['images/icon-user.png']) ?>" alt="user-icon" class="float-left"></a>
                    <div class="label-group">
                        <div class="label-1">Welcome</div>
                        <a href="#" class="view-info-user" id="<?= Yii::$app->user->identity->id ?>"><div class="label-2"><?= Yii::$app->user->identity->username ?></div></a>
                        <?php
                        Modal::begin([
                            'title' => '<h5>Detail User</h5>',
                            'id' => 'modal-view',
                            'size' => 'modal-md'
                        ]);
                        echo "<div id='modalViewContent'></div>"
                        ?>
                        <?php Modal::end(); ?>
                    </div>
                </div>
            </div>
            <!-- end user -->
            <ul class="list-group">
                <?php $action = Yii::$app->controller->id;?>
                <li class="list-group-item <?= ($action == 'movie-category' || $action == 'movie') ? 'active' : '' ?>" data-toggle="collapse" data-target="#menu-movie">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="item-menu">
                                <div class="row">
                                    <div class="col-md-2 d-flex align-items-center">
                                        <i class="fas fa-film icon-menu-collapse"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <span>Movie Management</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="navbar-collapse collapse hide" id="menu-movie" style="">
                        <ul class="list-group">
                            <li class="list-group-item <?= ($action == 'movie-category') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/movie-category']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Movie Category</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item <?= ($action == 'movie') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/movie']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Movie</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </li>

                <li class="list-group-item <?= ($action == 'seat-category' || $action == 'seat') ? 'active' : '' ?>" data-toggle="collapse" data-target="#menu-seat">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="item-menu">
                                <div class="row">
                                    <div class="col-md-2 d-flex align-items-center">
                                        <i class="fas fa-wheelchair icon-menu-collapse"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <span>Seat Management</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <!-- End Movie Management -->
                    <div class="navbar-collapse collapse hide" id="menu-seat" style="">
                        <ul class="list-group">
                            <li class="list-group-item <?= ($action == 'seat-category') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/seat-category']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Seat Category</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item <?= ($action == 'seat-line') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/seat-line']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Seat Line</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <li class="list-group-item <?= ($action == 'seat') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/seat']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Seat</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                        </ul>
                        <!-- End Seat Management -->
                    </div>
                </li>
                <li class="list-group-item <?= ($action == 'cinema-complex') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/cinema-complex']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="fas fa-building"></i>
                                </div>
                                <div class="col-md-10">
                                     <span>Complex Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!-- End Complex Management-->
                <li class="list-group-item <?= ($action == 'screen') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/screen']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="fas fa-desktop"></i>
                                </div>
                                <div class="col-md-10">
                                    <span>Screen Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!-- End Screen Management-->

                <li class="list-group-item <?= ($action == 'playtime' || $action == 'show-times '|| $action == 'show-times-detail') ? 'active' : '' ?>" data-toggle="collapse" data-target="#menu-show-times">
                    <ul class="list-group">
                        <li class="list-group-item">
                            <div class="item-menu">
                                <div class="row">
                                    <div class="col-md-2 d-flex align-items-center">
                                        <i class="fas fa-clock"></i>
                                    </div>
                                    <div class="col-md-10">
                                        <span>Show Times Management</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>

                    <div class="navbar-collapse collapse hide" id="menu-show-times" style="">
                        <ul class="list-group">
                            <li class="list-group-item <?= ($action == 'playtime') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/playtime']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Playtime</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!-- End Playtime -->
                            <li class="list-group-item <?= ($action == 'show-times') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/show-times']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Show Times</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!-- End Show Times -->
                            <li class="list-group-item <?= ($action == 'show-times-detail') ? 'active' : '' ?>">
                                <a href="<?= Url::to(['/show-times-detail']) ?>">
                                    <div class="item-menu">
                                        <div class="row">
                                            <div class="col-md-3 d-flex align-items-center pl-4">
                                                <i class="fas fa-arrow-right"></i>
                                            </div>
                                            <div class="col-md-9">
                                                <span>Show Times Detail</span>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </li>
                            <!-- End Show Times Detail-->
                        </ul>
                    </div>
                </li>
                <!-- End Show times Management -->

                <li class="list-group-item <?= ($action == 'booking') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/booking']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="fas fa-calendar-alt"></i>
                                </div>
                                <div class="col-md-10">
                                    <span>Booking Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!--End Booking Management-->
                <li class="list-group-item <?= ($action == 'user') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/user']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="fas fa-users"></i>
                                </div>
                                <div class="col-md-10">
                                    <span>User Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!-- End User Management-->
                <li class="list-group-item <?= ($action == 'slide') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/slide']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="fab fa-slideshare"></i>
                                </div>
                                <div class="col-md-10">
                                    <span>Slide Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!-- End Slide Management-->
                <li class="list-group-item <?= ($action == 'page') ? 'active' : '' ?>">
                    <a href="<?= Url::to(['/page']) ?>">
                        <div class="item-menu">
                            <div class="row">
                                <div class="col-md-2 d-flex align-items-center">
                                    <i class="far fa-file-alt"></i>
                                </div>
                                <div class="col-md-10">
                                    <span>Page Management</span>
                                </div>
                            </div>
                        </div>
                    </a>
                </li>
                <!-- End Page Management-->
            </ul>
            <!-- End list-group -->
        </div>
        <!-- End sidebar -->
        <div class="col-md-9half wrapper-content">
            <header>
                <div class="content-header d-flex align-items-center justify-content-between">
                    <span class="time"><?= date('d/m/Y h:i:s A', time()); ?></span>
                    <span class="float-right d-flex align-items-center">
                        <div class="btn-group">
                            <button type="button" class="btn btn-secondary dropdown-toggle btn-user" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                <img src="<?php echo Url::to(['images/icon-user-2.png']) ?>" alt="user-icon" class="pr-1 mb-1">
                                <?= Yii::$app->user->identity->username ?>
                            </button>
                            <div class="dropdown-menu dropdown-menu-right p-0">
                                <?php
                                echo ' <a data-method="post" style="text-decoration:none;" href="' . Url::to(['site/logout']) . '"><button class="dropdown-item pl-4 pt-2 pb-2 box-shadow shadow" type="button">Logout</button></a>';
                                ?>
                            </div>
                        </div>
                    </span>
                </div>
                <!-- end content-header -->
            </header>
            <!-- End header -->
            <div class="wp-inner">
                <div class="inner shadow">
                    <?= Alert::widget() ?>
                    <?php echo $content; ?>
                </div>
            </div>
        </div>
        <!-- wrapper-content -->
    </div>
    <!-- End row -->
</div>
<!-- End container -->


<?php $this->endBody() ?>
<script type='text/javascript'>//<![CDATA[
    function init() {
        var vidDefer = document.getElementsByTagName('iframe');
        for (var i = 0; i < vidDefer.length; i++) {
            if (vidDefer[i].getAttribute('data-src')) {
                vidDefer[i].setAttribute('src', vidDefer[i].getAttribute('data-src'));
            }
        }
    }
    window.onload = init;
    //]]></script>
</body>
</html>
<?php $this->endPage() ?>

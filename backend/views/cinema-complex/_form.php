<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\CinemaComplex */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="cinema-complex-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->isNewRecord ? 'create-form' : 'update-form',
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'name')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'address')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'phone_number')->textInput(['maxlength' => true]) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary' , 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

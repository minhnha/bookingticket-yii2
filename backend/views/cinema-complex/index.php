<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Cinema Complexes';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner-head">
    <?= Html::encode($this->title) ?>
</div>
<div class="inner-content">
    <div class="inner-top d-flex mb-3">
        <span class="title align-items-center">
            <img src="<?= Url::to(['images/bullet-room-list.png']) ?>" alt="bullet-cinema-complex-list" title="Cinema Complex List">Cinema Complex List
        </span>
        <div class="filter-data mt-1 ml-3">
            <div class="input-group search-data">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'method' => 'GET',
                    'action' => Url::to(['cinema-complex/search']),
                    'options' => [
                        'class' => 'form-inline'
                    ]
                ]) ?>
                <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search']) ?>
                <div class="input-group-append">
                    <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="btn-create ml-auto">
            <?= Html::button('<i class="fas fa-plus-circle"></i> Create', ['value' => Url::To(['cinema-complex/create']),'class' => 'btn btn-primary modalButtonCreate']) ?>
            <?php
            Modal::begin([
                'title' => '<h5>Create New Cinema Complex</h5>',
                'id' => 'modal-create',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalCreateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Update Cinema Complex</h5>',
                'id' => 'modal-update',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalUpdateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Detail Cinema Complex</h5>',
                'id' => 'modal-view',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalViewContent'></div>"
            ?>
            <?php Modal::end(); ?>
        </div>
    </div>
    <div class="inner-detail position-relative pb-5">
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => ['class' => 'table table-bordered table-hover table-responsive-md'],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'name',
                    'address',
                    'phone_number',
                    //'created_at',
                    //'created_by',
                    // 'updated_at',
                    // 'updated_by',

                    ['class' => 'yii\grid\ActionColumn',
                        'header' => 'Action',
                        'headerOptions' => [
                            'class' => 'text-center'
                        ],
                        'contentOptions' => [
                            'class' => 'text-center w-auto',
                            'style' => 'width:25%!important'
                        ],
                        'buttons' => [
                            'view' => function($url){
                                return Html::button('<i class="fas fa-eye"></i> View',[
                                    'class' => 'btn btn-success btn-xs modalButtonView',
                                    'value' => $url,
                                ]);
                            },
                            'update' => function($url){
                                return Html::button('<i class="fas fa-edit"></i> Edit',[
                                    'value' => $url,
                                    'class' => 'btn btn-primary btn-xs modalButtonUpdate',
                                ]);
                            },
                            'delete' => function($url, $model){
                                return Html::a('<i class="fas fa-trash-alt"></i> Delete', $url, [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-confirm' => 'Bạn có muốn xóa '.$model->name.' không?',
                                    'data-method' => 'POST'
                                ]);
                            }
                        ]
                    ],
                ],
                'pager' => [
                    'class' => 'justinvoelker\separatedpager\LinkPager',
                    'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                    'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                    'maxButtonCount' => 7,
                    'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
                ]
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

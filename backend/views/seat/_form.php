<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Seat */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="seat-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->isNewRecord ? 'create-form' : 'update-form',
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'screen_id')->dropDownList($model->getListScreen(), [
        'prompt' => '-- Choose screen --'
    ]) ?>

    <?= $form->field($model, 'seat_line_id')->dropDownList($model->getListSeatLine(), [
        'prompt' => '-- Choose line --'
    ]) ?>

    <?= $form->field($model, 'cat_id')->dropDownList($model->getListSeatCat(), [
        'prompt' => '-- Choose category --'
    ]) ?>

    <?= $form->field($model, 'seat_code')->textInput(['maxlength' => true]) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Seat */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Seats', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="seat-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'seat_code',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 0)
                        return 'Not booked';
                    return 'Booked';
                }
            ],
            'catTitle',
            'seatLineName',
            'screenName',
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

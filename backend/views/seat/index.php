<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;
/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Seats';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner-head">
    <?= Html::encode($this->title) ?>
</div>
<div class="inner-content">
    <div class="inner-top d-flex justify-content-between mb-3">
        <span class="title">
            <img src="<?= Url::to(['images/bullet-room-list.png']) ?>" alt="bullet-seat-category-list" title="Seat Category List">Seat List
        </span>
        <div class="filter-data mt-1 ml-3">
            <div class="input-group search-data">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'method' => 'GET',
                    'action' => Url::toRoute('search'),
                    'options' => [
                        'class' => 'form-inline'
                    ]
                ]) ?>
                    <?= $form->field($model, 'cat_id')->dropDownList($model->getListSeatCat(),
                    [
                        'prompt' => '-- Filter Category --',
                        'onchange' => 'requestData()',
                        'name' => 'cat_id',
                        'class' => 'mr-1 form-control',
                        'options' => [(isset($_GET['cat_id'])) ? $_GET['cat_id'] : '' => ['selected' => true]]
                    ])->label(false) ?>
                    <?= $form->field($model, 'screen_id')->dropDownList($model->getListScreen(),
                        [
                            'prompt' => '-- Filter Screen --',
                            'onchange' => 'requestData()',
                            'name' => 'screen_id',
                            'class' => 'mr-1 form-control',
                            'options' => [(isset($_GET['screen_id'])) ? $_GET['screen_id'] : '' => ['selected' => true]]
                        ])->label(false) ?>
                <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search data']) ?>
                <div class="input-group-append">
                    <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="btn-create ml-auto">
            <?= Html::button('<i class="fas fa-plus-circle"></i> Create', ['value' => Url::To(['seat/create']),'class' => 'btn btn-primary modalButtonCreate']) ?>
            <?php
            Modal::begin([
                'title' => '<h5>Create New Seat</h5>',
                'id' => 'modal-create',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalCreateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Update Seat</h5>',
                'id' => 'modal-update',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalUpdateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Detail Seat</h5>',
                'id' => 'modal-view',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalViewContent'></div>"
            ?>
            <?php Modal::end(); ?>

        </div>
    </div>
    <div class="inner-detail position-relative pb-5">
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'tableOptions' => [
                    'class' => 'table table-bordered table-hover table-responsive-md mb-3 data'
                ],
                'dataProvider' => $dataProvider,
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn', 'header' => 'ID'],
                    //'id',
                    'seat_code',
                    [
                        'attribute' => 'cat_id',
                        'value' => 'cat.title'
                    ],
                    [
                        'attribute' => 'seat_line_id',
                        'value' => 'seatLine.line'
                    ],
                    [
                        'attribute' => 'status',
                        'content' => function($model){
                            if($model->status == 0)
                                return '<span class="badge badge-pill badge-primary">Not booked</span>';
                            return '<span class="badge badge-pill badge-danger">Booked</span>';
                        }
                    ],
                    [
                        'attribute' => 'screen_id',
                        'value' => 'screen.name'
                    ],
                    // 'created_at',
                    // 'created_by',
                    // 'updated_at',
                    // 'updated_by',

                    ['class' => 'yii\grid\ActionColumn',
                        'header' => 'Action',
                        'headerOptions' => [
                            'class' => 'text-center'
                        ],
                        'contentOptions' => [
                            'class' => 'text-center'
                        ],
                        'buttons' => [
                            'view' => function($url){
                                return Html::button('<i class="fas fa-eye"></i> View',[
                                    'class' => 'btn btn-success btn-xs modalButtonView',
                                    'value' => $url,
                                ]);
                            },
                            'update' => function($url){
                                return Html::button('<i class="fas fa-edit"></i> Edit',[
                                    'value' => $url,
                                    'class' => 'btn btn-primary btn-xs modalButtonUpdate',
                                ]);
                            },
                            'delete' => function($url, $model){
                                return Html::a('<i class="fas fa-trash-alt"></i> Delete', $url, [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-confirm' => 'Bạn có muốn xóa '.$model->seat_code.' không?',
                                    'data-method' => 'POST'
                                ]);
                            }
                        ]
                    ],
                ],
                'pager' => [
                    'class' => 'justinvoelker\separatedpager\LinkPager',
                    'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                    'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                    'maxButtonCount' => 7,
                    'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
                ]
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

<?php
$url = Url::toRoute(['search', 'cat_id' => '']);
$this->registerJs(<<<JS
    // Get seat-cat_id, status to filter
    function requestData(){
        var cat_id = $('#seat-cat_id').val();
        var screen_id = $('#seat-screen_id').val();
        let URL = '$url';
        window.location.replace(URL + cat_id + '&screen_id=' + screen_id);
    }
JS
    , $this::POS_END);
?>

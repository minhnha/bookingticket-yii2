<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\datecontrol\DateControl;

/* @var $this yii\web\View */
/* @var $model common\models\ShowTimes */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="show-times-form">

    <?php $action = $model->isNewRecord ? 'create-form' : 'update-form' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true,
    ]); ?>

    <?= $form->field($model, 'date')->widget(DateControl::classname(), [
        'type' => 'date',
        'readonly' => true,
        'options' => ['id' => $action.'-dtp-date'],
        'widgetOptions' => [
            'pluginOptions' => [
                'autoclose' => true,
                'todayHighlight' => true,
                'startDate' => Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i'),
            ]
        ],
        'language' => 'vi'
    ]);
    ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>


<?php
$this->registerJs(<<<JS
    $('.invalid-feedback').addClass('d-block');
    $('#create-form-dtp-date').change(function() {
        let date = $(this).val();
        if(date == null){
            $('.invalid-feedback').text('Date cannot be blank.');
        }
    });
JS
    , $this::POS_READY);
?>
<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\Movie */

$this->title = $model->title;
$this->params['breadcrumbs'][] = ['label' => 'Movies', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="movie-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'title',
            'image:image',
            'cast',
            'director',
            'studio',
            'length',
            [
                'attribute' => 'version',
                'value' => function($model){
                    if($model->version == 0)
                        return '2D';
                    return '3D';
                }
            ],
            'content:ntext',
            'catTitle',
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == 0)
                        return 'Movie show';
                    return 'Movie Coming';
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'userCreate',
                'label' => 'Created by'
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
            [
                'attribute' => 'userUpdate',
                'label' => 'Updated by'
            ]
        ],
    ]) ?>

</div>

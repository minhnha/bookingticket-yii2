<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Movies';
$this->params['breadcrumbs'][] = $this->title;

$baseUrl = Yii::$app->urlManager->baseUrl;
?>
<div class="inner-head">
    <?= Html::encode($this->title) ?>
</div>
<div class="inner-content">
    <div class="inner-top d-flex justify-content-between mb-3">
        <span class="title">
            <img src="<?= Url::to(['images/bullet-room-list.png']) ?>" alt="bullet-movie-category-list" title="Movie Category List">Movie List
        </span>
        <div class="filter-data mt-1 ml-3">
            <div class="input-group search-data">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'method' => 'GET',
                    'action' => Url::toRoute('search'),
                    'options' => [
                        'class' => 'form-inline'
                    ]
                ]) ?>
                <?= $form->field($model, 'cat_id')->dropDownList($model->getMovieList(),
                    [
                        'prompt' => '-- Filter Category --',
                        'onchange' => 'requestData()',
                        'name' => 'cat_id',
                        'class' => 'mr-1 form-control',
                        'options' => [(isset($_GET['cat_id'])) ? $_GET['cat_id'] : '' => ['selected' => true]]
                    ])->label(false) ?>

                <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search data']) ?>
                <div class="input-group-append">
                    <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="btn-create ml-auto">
            <?= Html::button('<i class="fas fa-plus-circle"></i> Create', ['value' => Url::To(['movie/create']),'class' => 'btn btn-primary modalButtonCreate']) ?>
            <?php
            Modal::begin([
                'title' => '<h5>Create New Movie</h5>',
                'id' => 'modal-create',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalCreateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Update Movie</h5>',
                'id' => 'modal-update',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalUpdateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Detail Movie</h5>',
                'id' => 'modal-view',
                'size' => 'modal-lg'
            ]);
            echo "<div id='modalViewContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Image library</h5>',
                'id' => 'modal-media-image',
                'size' => 'modal-lg custom-modal-media'
            ]);
            echo '<iframe data-src="'.$baseUrl.'/file/dialog.php?field_id=image" frameborder="0" class="w-100" style="min-height: 550px"></iframe>';
            ?>
            <?php Modal::end(); ?>

        </div>
    </div>
    <div class="inner-detail position-relative pb-5">
    <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'tableOptions' => [
                'class' => 'table table-bordered table-hover table-responsive-md'
            ],
            'dataProvider' => $dataProvider,
            'columns' => [
                ['class' => 'yii\grid\SerialColumn', 'header' => 'ID'],
                //'id',
                'title',
                //'cast',
                //'director',
                //'studio',
                'attribute' => 'length',
                [
                    'attribute' => 'version',
                    'content' => function($model){
                        if($model->version == 0)
                            return '<span class="badge badge-pill badge-primary">2D</span>';
                        return '<span class="badge badge-pill badge-success">3D</span>';
                    }
                ],
                // 'content:ntext',
                [
                    'attribute' => 'status',
                    'content' => function($model){
                        if($model->status == 0)
                            return '<span class="badge badge-pill badge-primary">Movie show</span>';
                        return '<span class="badge badge-pill badge-success">Movie Coming</span>';
                    }
                ],
                [
                    'attribute' => 'cat_id',
                    'value' => 'movieCategory.title'
                ],
                // 'created_at',
                // 'created_by',
                // 'updated_at',
                // 'updated_by',

                ['class' => 'yii\grid\ActionColumn',
                    'header' => 'Action',
                    'headerOptions' => [
                        'class' => 'text-center'
                    ],
                    'contentOptions' => [
                        'class' => 'text-center w-auto',
                        'style' => 'width:25%!important'
                    ],
                    'buttons' => [
                        'view' => function($url){
                            return Html::button('<i class="fas fa-eye"></i> View',[
                                'class' => 'btn btn-success btn-xs modalButtonView',
                                'value' => $url,
                            ]);
                        },
                        'update' => function($url){
                            return Html::button('<i class="fas fa-edit"></i> Edit',[
                                'value' => $url,
                                'class' => 'btn btn-primary btn-xs modalButtonUpdate',
                            ]);
                        },
                        'delete' => function($url, $model){
                            return Html::a('<i class="fas fa-trash-alt"></i> Delete', $url, [
                                'class' => 'btn btn-danger btn-xs',
                                'data-confirm' => 'Bạn có muốn xóa '.$model->title.' không?',
                                'data-method' => 'POST'
                            ]);
                        }
                    ]
                ],
            ],
            'pager' => [
                'class' => 'justinvoelker\separatedpager\LinkPager',
                'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                'maxButtonCount' => 7,
                'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
            ]
        ]); ?>
    <?php Pjax::end(); ?>
    </div>
</div>

<?php
$url = Url::toRoute(['search', 'cat_id' => '' ]);
$this->registerJs(<<<JS
    // Get movie-cat_id to filter
    function requestData(){
        var cat_id = $('#movie-cat_id').val();
        let URL = '$url';
        window.location.replace(URL+cat_id);
    }
JS
    , $this::POS_END);
?>


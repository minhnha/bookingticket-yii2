<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\Movie */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="movie-form">

    <?php $form = ActiveForm::begin([
        'id' => $model->isNewRecord ? 'create-form' : 'update-form',
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'image', ['options' => ['class' => 'mb-0']])->hiddenInput(['id' => 'image']) ?>
    <div class="text-center"><img src="<?= $model->image ?>" alt="" class="show-img mb-3"></div>
    <a href="#" class="select-img btn btn-info btn-sm mt-0 mb-3 d-block" title="Choose image">Choose image</a>

    <?= $form->field($model, 'cast')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'director')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'studio')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'length')->textInput() ?>

    <?= $form->field($model, 'version')->radioList(
        [0 => '2D', 1 => '3D'],
        ['unselect' => null]
    ) ?>

    <?= $form->field($model, 'content')->textarea(['rows' => 6]) ?>

    <?= $form->field($model, 'cat_id')->dropDownList(
        $model->getMovieList(),
        ['prompt' => ' -- Choose category -- ']
    ) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>
</div>

<?php
$this->registerJs(<<<JS
        $(document).ready(function () {
            $('a.select-img').click(function() {
            event.preventDefault();
            $('#modal-media-image').modal('show');
            $('#modal-media-image').on('hide.bs.modal', function () {
                let imgUrl = $('input#image').val();
                $('img.show-img').attr('src', imgUrl);
            });
        });
    });
JS
    , $this::POS_HEAD);
?>

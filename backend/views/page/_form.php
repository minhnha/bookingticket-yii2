<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use dosamigos\ckeditor\CKEditor;

/* @var $this yii\web\View */
/* @var $model common\models\Page */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="page-form">
    <?php $action = $model->isNewRecord ? 'create-form' : 'update-form' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true
    ]); ?>

    <?= $form->field($model, 'title')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'slug')->textInput(['maxlength' => true]) ?>

    <?= $form->field($model, 'content')->widget(CKEditor::className(), [
        'options' => ['id' => $action.'ckeditor'],
        'preset' => 'stander',
        'clientOptions' => [
            'extraPlugins' => '',
            'height' => 500,

            //Here you give the action who will handle the image upload
            'filebrowserBrowseUrl' => 'file/dialog.php',
            'filebrowserUploadUrl' => 'uploads/file',
            'filebrowserImageBrowseUrl' => 'file/dialog.php',
            'filebrowserImageUploadUrl' => 'uploads/images',

            'toolbarGroups' => [
                ['name' => 'undo'],
                ['name' => 'basicstyles', 'groups' => ['basicstyles', 'cleanup']],
                ['name' => 'paragraph', 'groups' => ['list', 'indent', 'blocks', 'align', 'bidi' ]],
                ['name' => 'styles'],
                ['name' => 'links', 'groups' => ['links', 'insert']]
            ]

        ]
    ]) ?>

    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary' , 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

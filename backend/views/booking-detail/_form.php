<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model common\models\BookingDetail */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="booking-detail-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($model, 'booking_id')->textInput() ?>

    <?= $form->field($model, 'seat_id')->textInput() ?>

    <?= $form->field($model, 'status')->textInput() ?>

    <div class="form-group">
        <?= Html::submitButton($model->isNewRecord ? 'Create' : 'Update', ['class' => $model->isNewRecord ? 'btn btn-success' : 'btn btn-primary']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

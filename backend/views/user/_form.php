<?php

use yii\helpers\Html;
use yii\bootstrap4\ActiveForm;
use kartik\datecontrol\DateControl;


/* @var $this yii\web\View */
/* @var $model common\models\User */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="user-form">
    <?php $action = $model->isNewRecord ? 'create-form' : 'update-form' ?>
    <?php $form = ActiveForm::begin([
        'id' => $action,
        'enableAjaxValidation' => true,
    ]); ?>

    <div class="row">
        <div class="col-md-6">
            <?= $form->field($model, 'full_name')->textInput()->label('Full Name <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'username')->textInput()->label('Username <span class="required-field">(*)</span>') ?>

            <?php if($action == 'create-form'): ?>
                <?= $form->field($model, 'password')->passwordInput()->label('Password <span class="required-field">(*)</span>') ?>
            <?php endif; ?>

            <?= $form->field($model, 'email')->input('email')->label('Email <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'phone_number')->textInput()->label('Phone Number <span class="required-field">(*)</span>') ?>

            <?= $form->field($model, 'password')->passwordInput()->label('Password <span class="required-field">(*)</span>') ?>

        </div>
        <div class="col-md-6">
            <?= $form->field($model, 'gender')->radioList(
                [0 => 'Male', 1 => 'Female']
            ) ?>

            <?=
             $form->field($model, 'birthday')->widget(DateControl::classname(), [
                'readonly' => true,
                'type' => 'date',
                'options' => ['id' => $action.'-dtp-birthday'],
                'widgetOptions' => [
                    'pluginOptions' => [
                        'autoclose' => true,
                        'todayHighlight' => true,
                        // 'startDate' => Yii::$app->formatter->asDatetime('now', 'php:Y-m-d H:i'),
                    ]
                ],
                'language' => 'vi'
            ]);
            ?>

            <?= $form->field($model, 'address')->textarea() ?>

            <?= $form->field($model, 'permission')->radioList(
                [10 => 'Admin', 0 => 'Customer'],
                ['unselect' => null]
            )->label('Permission <span class="required-field">(*)</span>') ?>
        </div>
    </div>
    <hr>
    <div class="form-group d-flex justify-content-end">
        <?= Html::submitButton($model->isNewRecord ? '<i class="fas fa-save"></i> Create' : '<i class="fas fa-save"></i> Update', ['class' => $model->isNewRecord ? 'btn btn-success mr-2' : 'btn btn-primary mr-2']) ?>
        <?= Html::button('Close', ['class' => 'btn btn-secondary', 'data-dismiss' => 'modal']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>

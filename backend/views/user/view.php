<?php

use yii\helpers\Html;
use yii\widgets\DetailView;

/* @var $this yii\web\View */
/* @var $model common\models\User */

$this->title = $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Users', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="user-view">

    <?= DetailView::widget([
        'model' => $model,
        'attributes' => [
            'id',
            'username',
            'full_name',
            'email:email',
            [
                'attribute' => 'gender',
                'value' => function($model){
                    if($model->gender == \common\models\User::MALE_GENDER)
                        return 'Male';
                    return 'Female';
                }
            ],
            [
                'attribute' => 'birthday',
                'value' => $model->formatDate($model->birthday)
            ],
            'address',
            'phone_number',
            [
                'attribute' => 'permission',
                'value' => function($model){
                    if($model->permission == \common\models\User::ADMIN_PERMISSION)
                        return 'Admin';
                    return 'Customer';
                }
            ],
            [
                'attribute' => 'status',
                'value' => function($model){
                    if($model->status == \common\models\User::STATUS_ACTIVE)
                        return 'Active';
                    return 'None Active';
                }
            ],
            [
                'attribute' => 'created_at',
                'value' => $model->formatDateTime($model->created_at)
            ],
            [
                'attribute' => 'updated_at',
                'value' => $model->formatDateTime($model->updated_at)
            ],
        ],
    ]) ?>

</div>

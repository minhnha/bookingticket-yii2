<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\Pjax;
use yii\helpers\Url;
use yii\bootstrap4\Modal;
use yii\bootstrap4\ActiveForm;


/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Users';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="inner-head">
    <?= Html::encode($this->title) ?>
</div>
<div class="inner-content">
    <div class="inner-top d-flex mb-3">
        <span class="title align-items-center">
            <img src="<?= Url::to(['images/bullet-room-list.png']) ?>" alt="bullet-user-list" title="User List">User List
        </span>
        <div class="filter-data mt-1 ml-3">
            <div class="input-group search-data">
                <?php $form = ActiveForm::begin([
                    'enableClientValidation' => false,
                    'method' => 'GET',
                    'action' => Url::to(['user/search']),
                    'options' => [
                        'class' => 'form-inline'
                    ]
                ]) ?>
                <?= Html::textInput('value', null, ['class' => 'form-control', 'placeholder' => 'Search']) ?>
                <div class="input-group-append">
                    <?= Html::submitButton('<i class="fas fa-search"></i>', ['class' => 'btn btn-outline-secondary btn-search']) ?>
                </div>
                <?php ActiveForm::end(); ?>
            </div>
        </div>
        <div class="btn-create ml-auto">
            <?= Html::button('<i class="fas fa-plus-circle"></i> Create', ['value' => Url::To(['user/create']),'class' => 'btn btn-primary modalButtonCreate']) ?>
            <?php
            Modal::begin([
                'title' => '<h5>Create New User</h5> <span class="text-danger ml-4 pt-1">Field (*): Required</span>',
                'id' => 'modal-create',
                'size' => 'modal-lg'
            ]);
            echo "<div id='modalCreateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Update User</h5> <span class="text-danger ml-4 pt-1">Field (*): Required</span>',
                'id' => 'modal-update',
                'size' => 'modal-lg'
            ]);
            echo "<div id='modalUpdateContent'></div>"
            ?>
            <?php Modal::end(); ?>

            <?php
            Modal::begin([
                'title' => '<h5>Detail User</h5>',
                'id' => 'modal-view',
                'size' => 'modal-md'
            ]);
            echo "<div id='modalViewContent'></div>"
            ?>
            <?php Modal::end(); ?>
        </div>
    </div>
    <div class="inner-detail position-relative pb-5">
        <?php Pjax::begin(); ?>
            <?= GridView::widget([
                'dataProvider' => $dataProvider,
                'tableOptions' => [
                    'class' => 'table-hover table table-bordered'
                ],
                'columns' => [
                    ['class' => 'yii\grid\SerialColumn'],
                    //'id',
                    'username',
                    //'auth_key',
                    //'password_hash',
                    //'password_reset_token',
                    'email:email',
                    // 'gender',
                    // 'birthday',
                    // 'address',
                    'phone_number',
                    [
                        'attribute' => 'permission',
                        'content' => function($model){
                            if($model->permission == 10)
                                return '<span class="badge badge-pill badge-success">Admin</span>';
                            else return '<span class="badge badge-pill badge-primary">Customer</span>';
                        }
                    ],
                    // 'status',
                    // 'active_token',
                    // 'created_at',
                    // 'updated_at',

                    ['class' => 'yii\grid\ActionColumn',
                        'header' => 'Action',
                        'headerOptions' => [
                            'class' => 'text-center'
                        ],
                        'contentOptions' => [
                            'class' => 'text-center'
                        ],
                        'buttons' => [
                            'view' => function($url){
                                return Html::button('<i class="fas fa-eye"></i> View',[
                                    'class' => 'btn btn-success btn-xs modalButtonView',
                                    'value' => $url,
                                ]);
                            },
                            'update' => function($url){
                                return Html::button('<i class="fas fa-edit"></i> Edit',[
                                    'value' => $url,
                                    'class' => 'btn btn-primary btn-xs modalButtonUpdate',
                                ]);
                            },
                            'delete' => function($url, $model){
                                return Html::a('<i class="fas fa-trash-alt"></i> Delete', $url, [
                                    'class' => 'btn btn-danger btn-xs',
                                    'data-confirm' => 'Bạn có muốn xóa '.$model->username.' không?',
                                    'data-method' => 'POST'
                                ]);
                            }
                        ]
                    ],
                ],
                'pager' => [
                    'class' => 'justinvoelker\separatedpager\LinkPager',
                    'prevPageLabel' => '<span class="fas fa-angle-left"></span>',
                    'nextPageLabel' => '<span class="fas fa-angle-right"></span>',
                    'maxButtonCount' => 7,
                    'options' => ['class' => 'pagination justify-content-end mr-3 position-absolute'],
                ]
            ]); ?>
        <?php Pjax::end(); ?>
    </div>
</div>

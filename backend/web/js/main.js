$(document).ready(function(){
    // when click button create
    $('.modalButtonCreate').click(function(){
        $('#modal-create').modal('show')
            .find('#modalCreateContent')
            .load($(this).attr('value'));
    });

    // $('.view-info-user').click(function () {
    //     event.preventDefault();
    //     $('#modal-view').modal('show');
    //
    // });
});


$(window).on('load pjax:success',function () {
    // when click button update
    $('.modalButtonUpdate').click(function(){
        $('#modal-update').modal('show')
            .find('#modalUpdateContent')
            .load($(this).attr('value'));
    });

    // when click button view
    $('.modalButtonView').click(function(){
        $('#modal-view').modal('show')
            .find('#modalViewContent')
            .load($(this).attr('value'));
    });

});
<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-backend',
    'basePath' => dirname(__DIR__),
    'controllerNamespace' => 'backend\controllers',
    'bootstrap' => ['log'],
    'modules' => [],
    'components' => [
        'request' => [
            'csrfParam' => '_csrf-backend',
        ],
        'user' => [
            'identityClass' => 'common\models\User',
            'enableAutoLogin' => true,
            'identityCookie' => ['name' => '_identity-backend', 'httpOnly' => true],
        ],
        'session' => [
            // this is the name of the session cookie used for login on the backend
            'name' => 'advanced-backend',
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '<controller:\w+>/<id:\d+>' => '<controller>/view',
                '<controller:\w+>/<action:\w+>/<id:\d+>' => '<controller>/<action>',
                '<controller:\w+>/<action:\w+>' => '<controller>/<action>',
            ],
        ],
        /*
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
            ],
        ],
        */
    ],
    'params' => $params,

//    'as globalAccess'=>[
//        'class'=>'\common\behaviors\GlobalAccessBehavior',
//
//        'rules'=>[
//            [
//                'controllers'=>['gii/default'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['@']
//            ],
//            [
//                'controllers'=>['debug/default'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['booking'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['movie-category'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['movie'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['seat-category'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['seat'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['seat-line'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['page'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['slide'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['playtime'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['cinema-complex'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['screen'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['user'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['show-times'],
//                'allow' => true,
//                'roles' => ['@'],
//            ],
//            [
//                'controllers'=>['show-times-detail'],
//                'allow' => true,
//                'roles' => ['@'],
//                //'actions'=>['index']
//            ],
//            [
//                'controllers'=>['site'],
//                'allow' => true,
//                'roles' => ['@'],
//                'actions'=>['index']
//            ],
//            [
//                'controllers'=>['site'],
//                'allow' => true,
//                'roles' => ['@'],
//                'actions'=>['logout']
//            ],
//            [
//                'controllers'=>['site'],
//                'allow' => true,
//                'roles' => ['?', '@'],
//                'actions'=>['error']
//            ],
//            [
//                'controllers'=>['site'],
//                'actions'=>['login'],
//                'allow' =>TRUE,
//                'roles'=>['?'],
//            ],
//        ]
//    ],
];

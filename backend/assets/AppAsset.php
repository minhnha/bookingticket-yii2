<?php

namespace backend\assets;

use yii\web\AssetBundle;

/**
 * Main backend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        //'css/site.css',
        'css/bootstrap.min.css',
        'css/gly-icon.css',
        //'css/font-awesome.5.3.1.css',
        'css/fonts/font-awesome/css/font-awesome.5.1.min.css',
        'css/style-manage.css',
    ];
    public $js = [
        'js/popper.min.js',
        'js/bootstrap.min.js',
        //'module/ckeditor/ckeditor.js',
        'js/main.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap4\BootstrapAsset',
    ];
}

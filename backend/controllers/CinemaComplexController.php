<?php

namespace backend\controllers;

use Yii;
use common\models\CinemaComplex;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * CinemaComplexController implements the CRUD actions for CinemaComplex model.
 */
class CinemaComplexController extends Controller
{
    /**
     * Lists all CinemaComplex models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => CinemaComplex::find(),
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single CinemaComplex model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new CinemaComplex model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new CinemaComplex();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            if ($model->save())
                Yii::$app->session->setFlash('success', "Create <b>$model->name</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create <b>' . $model->name . '</b>');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing CinemaComplex model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update <b>$model->name</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing CinemaComplex model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch($value)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => CinemaComplex::find()->where([
                    'like', 'name', $value,
                    'like', 'address', $value,
                    'like', 'phone_number', $value
                ])
            ]);
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the CinemaComplex model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return CinemaComplex the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = CinemaComplex::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }
}

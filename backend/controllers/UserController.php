<?php

namespace backend\controllers;
//ob_start();

use Yii;
use common\models\User;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * UserController implements the CRUD actions for User model.
 */
class UserController extends Controller
{
    /**
     * Lists all User models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => User::find(),
        ]);

        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single User model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new User model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new User();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        $model->scenario = 'create';
        if($model->load(Yii::$app->request->post())){
            if ($model->save())
                Yii::$app->session->setFlash('success', "Create <b>$model->username</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create <b>' . $model->username . '</b>');
            return $this->redirect(['index']);
        }
        //$model->birthday = $model->birthday + 25200;
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing User model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        Yii::$app->setTimeZone('Asia/Ho_Chi_Minh');
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if($model->password != null){
                $model->updatePassword($model->password);
            }
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update <b>$model->username</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        //$model->birthday = $model->birthday + 25200;
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing User model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch($value = null)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => User::find()->where([
                    'OR',
                    ['like', 'username', $value],
                    ['like', 'full_name', $value],
                    ['like', 'email', $value],
                    ['like', 'phone_number', $value],
                    ['like', 'FROM_UNIXTIME(`birthday`)', $value]
                ])
            ]);
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the User model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return User the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = User::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }
}

<?php

namespace backend\controllers;

use Yii;
use common\models\ShowTimes;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * ShowTimesController implements the CRUD actions for ShowTimes model.
 */
class ShowTimesController extends Controller
{
    /**
     * Lists all ShowTimes models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => ShowTimes::find()->orderBy('date'),
        ]);

        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single ShowTimes model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShowTimes model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShowTimes();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){

            if ($model->save())
                Yii::$app->session->setFlash('success', "Create <b>".$model->formatDate($model->date)."</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create this <b>show times</b>.');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShowTimes model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update <b>".$model->formatDate($model->date)."</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    public function actionSearch($value)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => ShowTimes::find()->where([
                    'or',
                    ['like', 'FROM_UNIXTIME(`date`, "%d/%m/%Y")', $value]
                ])
            ]);
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Deletes an existing ShowTimes model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        return $this->redirect(['index']);
    }

    /**
     * Finds the ShowTimes model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShowTimes the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShowTimes::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }

//    public function actionGetErrors(){
//        $model = new ShowTimes();
//        if(isset($_POST['ajax']) && $_POST['ajax']==='create-form')
//        {
//            $this->asJson($_POST['date-create-form-dtp-date-disp']);
//
//            echo $model->validate();
//
//            return Yii::$app->end();
//        }
//    }
}

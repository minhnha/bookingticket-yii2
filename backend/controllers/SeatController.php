<?php

namespace backend\controllers;

use Yii;
use common\models\Seat;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * SeatController implements the CRUD actions for Seat model.
 */
class SeatController extends Controller
{
    /**
     * Lists all Seat models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new Seat();
        $dataProvider = new ActiveDataProvider([
            'query' => $model::find(),
        ]);
        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single Seat model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Seat model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Seat();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            if ($model->save())
                Yii::$app->session->setFlash('success', "Create <b>$model->seat_code</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create <b>' . $model->seat_code . '</b>');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Seat model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if($model->save()){
                Yii::$app->session->setFlash('success', "Update <b>$model->seat_code</b> successful.");
                return $this->redirect(['index']);
            }else{
                Yii::$app->session->setFlash('error', "Error: " .print_r($model->getErrors()));
                return $this->redirect(['index']);
            }
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Seat model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect(['index']);
    }

    public function actionSearch($cat_id = null, $screen_id = null, $value = null)
    {
        $model = new Seat();
        if ($cat_id != null && $screen_id == null && $value == null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where(['cat_id' => $cat_id])
            ]);
        elseif($cat_id != null && $screen_id != null && $value == null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where(['cat_id' => $cat_id])->andWhere(['screen_id' => $screen_id])
            ]);
        elseif($cat_id == null && $screen_id != null && $value == null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where(['screen_id' => $screen_id])
            ]);
        elseif($cat_id != null && $screen_id == null && $value != null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where([
                    'cat_id' => $cat_id
                ])->andWhere([
                    'OR',
                    ['like', 'seat_code', $value],
                    ['like', 'screen_id', $value],
                    ['like', 'seat_line_id', $value],
                    ['like', 'cat_id', $value]
                ])
            ]);
        elseif ($cat_id == null && $screen_id == null && $value != null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where([
                    'OR',
                    ['like', 'seat_code', $value],
                    ['like', 'screen_id', $value],
                    ['like', 'seat_line_id', $value],
                    ['like', 'cat_id', $value]
                ])
            ]);
        elseif($cat_id == null && $screen_id != null && $value != null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where([
                    'screen_id' => $screen_id
                ])->andWhere([
                    'OR',
                    ['like', 'seat_code', $value],
                    ['like', 'screen_id', $value],
                    ['like', 'seat_line_id', $value],
                    ['like', 'cat_id', $value]
                ])
            ]);
        elseif($cat_id != null && $screen_id != null && $value != null):
            $dataProvider = new ActiveDataProvider([
                'query' => $model::find()->where([
                    'screen_id' => $screen_id
                ])->andWhere([
                    'cat_id' => $cat_id
                ])->andWhere([
                    'OR',
                    ['like', 'seat_code', $value],
                    ['like', 'screen_id', $value],
                    ['like', 'seat_line_id', $value],
                    ['like', 'cat_id', $value]
                ])
            ]);
        else:
            return $this->actionIndex();
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Finds the Seat model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Seat the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Seat::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }
}

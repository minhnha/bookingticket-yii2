<?php

namespace backend\controllers;

use Yii;
use common\models\Slide;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * SlideController implements the CRUD actions for Slide model.
 */
class SlideController extends Controller
{
    /**
     * Lists all Slide models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Slide::find(),
        ]);

        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Displays a single Slide model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Slide model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Slide();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){

            if ($model->save())
                Yii::$app->session->setFlash('success', "Create <b>$model->title</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create <b>' . $model->title . '</b>');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing Slide model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update <b>$model->title</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing Slide model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->addFlash('success', 'Delete '.$model->title.' successful.');
        return $this->redirect(['index']);
    }

    public function actionSearch($value)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => Slide::find()->where([
                    'or',
                    ['like', 'title', $value],
                    ['like', 'content', $value]
                ])
            ]);
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }

    /**
     * Finds the Slide model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Slide the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Slide::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }
}

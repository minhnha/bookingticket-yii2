<?php

namespace backend\controllers;

use common\models\BookingDetail;
use common\models\SeatCategory;
use Yii;
use common\models\Booking;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;
use common\models\ShowTimesDetail;
use common\models\Seat;

ob_start();

/**
 * BookingController implements the CRUD actions for Booking model.
 */
class BookingController extends Controller
{
    /**
     * Lists all Booking models.
     * @return mixed
     */
    public function actionIndex()
    {
        $dataProvider = new ActiveDataProvider([
            'query' => Booking::find(),
        ]);

        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }

    /**
     * Displays a single Booking model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new Booking model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new Booking();
        $showTimes = new ShowTimesDetail();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            if ($model->save())
                Yii::$app->session->setFlash('success', "Create booking ID: <b>$model->id</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Not create <b>' . $model->id . '</b>');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
            'showTimes' => $showTimes
        ]);
    }

    /**
     * Updates an existing Booking model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        $showTimes = new ShowTimesDetail();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update booking ID: <b>$model->id</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
            'showTimes' => $showTimes
        ]);
    }

    public function actionSearchByCinemaComplex($complex_id)
    {
        $query = new Query();
        $query ->select(['std.show_times_id as id', 'FROM_UNIXTIME(st.date, "%d/%m/%Y") date'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%show_times}} st',
                'st.id = std.show_times_id'
            )
            ->where(['std.cine_complex_id' => $complex_id])
            ->groupBy('std.show_times_id');
        $query->createCommand();
        $data = $query->all();

        $listData = '<option value="">--- Choose Date ---</option>';
        foreach ($data as $item):
            $listData .= '<option value="'.$item['id'].'">'.$item['date'].'</option>';
        endforeach;

        $this->asJson($listData);
    }

    public function actionSearchByShowTimes($complex_id, $show_times_id)
    {
        $query = new Query();
        $query ->select(['std.screen_id as id', 'sc.name as screen_name'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%screen}} sc',
                'sc.id = std.screen_id'
            )
            ->where(['std.cine_complex_id' => $complex_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->groupBy('std.screen_id');
        $query->createCommand();
        $data = $query->all();

        $listData = '<option value="">--- Choose Screen ---</option>';
        foreach ($data as $item):
            $listData .= '<option value="'.$item['id'].'">'.$item['screen_name'].'</option>';
        endforeach;

        $this->asJson($listData);
    }

    public function actionSearchByScreen($complex_id, $show_times_id, $screen_id)
    {
        $query = new Query();
        $query ->select(['std.movie_id as id', 'mv.title as movie_title'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%movie}} mv',
                'mv.id = std.movie_id'
            )
            ->where(['std.cine_complex_id' => $complex_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->andWhere(['std.screen_id' => $screen_id])
            ->groupBy('std.movie_id');
        $query->createCommand();
        $data = $query->all();

        $listData = '<option value="">--- Choose Movie ---</option>';
        foreach ($data as $item):
            $listData .= '<option value="'.$item['id'].'">'.$item['movie_title'].'</option>';
        endforeach;

        $this->asJson($listData);
    }

    public function actionSearchByMovie($complex_id, $show_times_id, $screen_id, $movie_id)
    {
        $query = new Query();
        $query ->select(['std.playtime_id as id', 'FROM_UNIXTIME(pt.start_time+3600, "%H:%i") start_time', 'FROM_UNIXTIME(pt.end_time+3600, "%H:%i") end_time'])
            ->from('{{%show_times_detail}} std')
            ->join('inner join',
                '{{%playtime}} pt',
                'pt.id = std.playtime_id'
            )
            ->where(['std.cine_complex_id' => $complex_id])
            ->andWhere(['std.show_times_id' => $show_times_id])
            ->andWhere(['std.screen_id' => $screen_id])
            ->andWhere(['std.movie_id' => $movie_id])
            ->groupBy('std.movie_id');
        $query->createCommand();
        $data = $query->all();

        $listData = '<option value="">--- Choose Playtime ---</option>';
        foreach ($data as $item):
            $listData .= '<option value="'.$item['id'].'">'.$item['start_time'].'-'.$item['end_time'].'</option>';
        endforeach;

        $this->asJson($listData);
    }

    public function actionGetIdShowTimesDetail($complex_id, $show_times_id, $screen_id, $movie_id, $playtime_id)
    {
        $idShowTimes = ShowTimesDetail::findOne([
            'cine_complex_id' => $complex_id,
            'show_times_id' => $show_times_id,
            'screen_id' => $screen_id,
            'movie_id' => $movie_id,
            'playtime_id' => $playtime_id
        ])->id;

        echo '<input type="hidden" id="getShowTimesId" value="'.$idShowTimes.'">';

        $booking = new Booking();
        $listSeat = $booking->getListSeatById($idShowTimes);
        foreach ($listSeat as $seatLine => $items):
            echo '<div class="d-flex bd-highlight mb-2 justify-content-center">
            <ul class="list-seat-code pl-0">
                <li class="mr-5 text-center">'.$seatLine.'</li>
                <li class="p-0">
                    <ul class="sub-list p-0 text-center">
                ';
                    $statusSeat = '';
                    foreach ($items as $id => $seatCode):
                        $status = BookingDetail::findOne(['show_times_detail_id' => $idShowTimes, 'seat_id' => $id])->status;
                        ($status == 1) ? $statusSeat = 'booked' : $statusSeat = '';
                        $catId = Seat::findOne(['id' => $id])->cat_id;
                        $catSeat = strtolower(SeatCategory::findOne(['id' => $catId])->title);
                        echo '<li class="'.$catSeat.' '.$statusSeat.'" value="'.$id.'">'.$seatLine.$seatCode.'</li>';
                    endforeach;
            echo '
                    </ul>
                </li>
            </ul>
        </div>
        <!-- End list seat -->';
        endforeach;

        echo "<script>
                $(document).ready(function() {
                      // Checked and choose Seat
                      $('ul.sub-list li').click(function() {
                            $(this).toggleClass('checked');
                      });
                      $('.btn-SeatChecked').click(function() {
                            let hClass = $('ul.sub-list li').hasClass('checked');
                            if(hClass){
                                let ArrValue = [];
                                $('li.checked').each(function() {
                                    let value = $(this).val();
                                    ArrValue.push( value );
                                });
                                $('#booking-seat_code').val(ArrValue);
                                $('#seatChecked').val(ArrValue);
                                $('#chooseSeat').modal('hide');
                            }
                            $('.btn-delete-seat').removeAttr('disabled');
                            $('.btn-choose-seat').attr('disabled',true);
                      });
                });
            </script>";
    }

    /**
     * Deletes an existing Booking model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->addFlash('success', 'Delete successful.');
        return $this->redirect(['index']);
    }

    /**
     * Finds the Booking model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return Booking the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = Booking::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }

    public function actionPriceBySeatId($listSeat)
    {
        // get price from listSeat
        $listSeat =  array_map('intval', explode(',', $listSeat));
        $query = new Query();
        $query ->select(['title', 'COUNT(title) qty', '(COUNT(title) * price) price'])
            ->from('{{%seat}} s')
            ->join('inner join',
                '{{%seat_category}} scat',
                'scat.id = s.cat_id'
            )
            ->where(['in', 's.id', $listSeat])
            ->groupBy('title');
        $query->createCommand();
        $data = $query->all();

        // get lineSeat from listSeat
        $query2 = New Query();
        $query2->select(['CONCAT(sl.line,s.seat_code) as list_seat_code'])
            ->from(['{{%seat}} s'])
            ->join(
                'inner join',
                '{{%seat_line}} sl',
                's.seat_line_id = sl.id'
            )
            ->where(['in', 's.id', $listSeat]);
        $query2->createCommand();
        $dataSeat = $query2->all();

        $listData2 = [];
        foreach ($dataSeat as $item):
            $listData2[] = $item['list_seat_code'];
        endforeach;
        implode(",", $listData2);

        $listData = [];
        $listData['price_total'] = 0;

        foreach ($data as $item):
            $listData[$item['title']] = $item['qty'];
            $listData['price_total'] += $item['price'];
        endforeach;

        $this->asJson([
            'listData' => $listData,
            'listSeatLine' => $listData2
        ]);
    }

    public function actionGetDataUpdate($show_times_detail_id)
    {
        $query = new Query();
        $query ->select(['std.show_times_id', 'std.cine_complex_id', 'std.screen_id', 'std.movie_id', 'std.playtime_id'])
            ->from('{{%show_times_detail}} std')
            ->where(['id' => $show_times_detail_id]);
        $query->createCommand();
        $data = $query->all();
        $listData = [];
        foreach ($data as $item):
            $listData['dataUpdate'] = $item;
        endforeach;

        $this->asJson([
            'dataUpdate' => $listData['dataUpdate']
        ]);
    }

    public function actionSearch($value = null)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => Booking::find()->where([
                    'OR',
                    ['like', 'phone_number', $value],
                    ['like', 'full_name', $value],
                ])
            ]);
        endif;

        $seat = new Seat();
        $listSeat = $seat->getListSeatByScreen();
        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
            'listSeat' => $listSeat
        ]);
    }
}

<?php

namespace backend\controllers;

use common\models\BookingDetail;
use common\models\Screen;
use common\models\Seat;
use Yii;
use common\models\ShowTimesDetail;
use yii\data\ActiveDataProvider;
use yii\db\Query;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\web\Response;
use yii\bootstrap4\ActiveForm;

/**
 * ShowTimesDetailController implements the CRUD actions for ShowTimesDetail model.
 */
class ShowTimesDetailController extends Controller
{
    /**
     * Lists all ShowTimesDetail models.
     * @return mixed
     */
    public function actionIndex()
    {
        $model = new ShowTimesDetail();
        $dataProvider = new ActiveDataProvider([
            'query' => ShowTimesDetail::find(),
        ]);

        $dataProvider->pagination->pageSize = 5;

        return $this->render('index', [
            'dataProvider' => $dataProvider,
            'model' => $model
        ]);
    }

    /**
     * Displays a single ShowTimesDetail model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->renderAjax('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new ShowTimesDetail model.
     * If creation is successful, the browser will be redirected to the 'view' slide.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new ShowTimesDetail();
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if($model->load(Yii::$app->request->post())){
            $screen_id = Yii::$app->request->post()["ShowTimesDetail"]['screen_id'];
            $screen = Screen::findOne(['id' => $screen_id]);
            $model->normal_seat_remaining = $screen->normal_seat_quantity;
            $model->vip_seat_remaining = $screen->vip_seat_quantity;
            if ($model->save())
                Yii::$app->session->setFlash('success', "Create Time ID: <b>".$model->id."</b> successful.");
            else
            {
                echo '<pre>';
                print_r($model->getErrors());die;
            }
                Yii::$app->session->setFlash('error', 'Error: '.print_r($model->getErrors()).'');
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing ShowTimesDetail model.
     * If update is successful, the browser will be redirected to the 'view' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        $model = $this->findModel($id);
        if (Yii::$app->request->isAjax && $model->load(Yii::$app->request->post())) {
            Yii::$app->response->format = Response::FORMAT_JSON;
            return ActiveForm::validate($model);
        }
        if ($model->load(Yii::$app->request->post())) {
            $screen_id = Yii::$app->request->post()["ShowTimesDetail"]['screen_id'];
            $screen = Screen::findOne(['id' => $screen_id]);
            $model->normal_seat_remaining = $screen->normal_seat_quantity;
            $model->vip_seat_remaining = $screen->vip_seat_quantity;
            if ($model->save())
                Yii::$app->session->setFlash('success', "Update Time ID: <b>".$model->id."</b> successful.");
            else
                Yii::$app->session->setFlash('error', 'Error: ' . print_r($model->getErrors()));
            return $this->redirect(['index']);
        }
        return $this->renderAjax('_form', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing ShowTimesDetail model.
     * If deletion is successful, the browser will be redirected to the 'index' slide.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();
        Yii::$app->session->addFlash('success', 'Delete this show times successful.');
        return $this->redirect(['index']);
    }

    public function actionSearchByCinemaComplex($complex_id){
        $query = new Query();
        $query ->select(['sc.id', 'sc.name'])
            ->from('{{%cinema_complex}} cc')
            ->join('inner join',
                '{{%screen}} sc',
                'cc.id = sc.cine_complex_id'
            )
            ->where(['cc.id' => $complex_id]);
        $query->createCommand();
        $data = $query->all();

        $listData = '<option value="">--- Choose Screen ---</option>';
        foreach ($data as $item):
            $listData .= '<option value="'.$item['id'].'">'.$item['name'].'</option>';
        endforeach;

        $this->asJson($listData);
    }

    /**
     * Finds the ShowTimesDetail model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return ShowTimesDetail the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = ShowTimesDetail::findOne($id)) !== null) {
            return $model;
        } else {
            throw new NotFoundHttpException('The requested slide does not exist.');
        }
    }

    public function actionGetScreenById($id)
    {
        $screen_id = ShowTimesDetail::findOne(['id' => $id])->screen_id;
        echo $screen_id;
    }

    public function actionSearch($value = null)
    {
        if ($value == null):
            return $this->actionIndex();
        else:
            $dataProvider = new ActiveDataProvider([
                'query' => ShowTimesDetail::find()->where([
                    'OR',
                    ['like', 'cine_complex_id', $value],
                    ['like', 'FROM_UNIXTIME(`show_times_id`, "%d/%m/%Y")', $value],
                    ['like', 'screen_id', $value],
                    ['like', 'movie_id', $value],
                    ['like', 'FROM_UNIXTIME(`playtime_id`+3600, "%H:%i")', $value]
                ])
            ]);
        endif;

        $dataProvider->pagination->pageSize = 5;
        return $this->render('index',[
            'dataProvider' => $dataProvider,
        ]);
    }
}

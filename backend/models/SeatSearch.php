<?php

namespace backend\models;

use common\models\Seat;
use yii\data\ActiveDataProvider;
use yii\db\conditions\LikeCondition;
use yii\db\conditions\OrCondition;

/**
 * @property string q
 */
class SeatSearch extends Seat
{
    public $q;
    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['room_id'], 'integer'],
            [['q'], 'string', 'max' => 255]
        ];
    }
    public function search($params)
    {
        $query = Seat::find();
        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        $query->andFilterWhere(['cat_id' => $this->cat_id]);
        $query->andFilterWhere(
            ['or',
                ['LIKE','seat_code',$this->q],
                ['LIKE','screen_id',$this->q],
                ['LIKE','seat_line_id',$this->q],
                ['LIKE','cat_id',$this->q],
            ]
        );
        return $dataProvider;
    }
}
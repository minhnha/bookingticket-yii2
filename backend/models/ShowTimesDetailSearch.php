<?php

namespace backend\models;

use Yii;
use yii\base\Model;
use yii\data\ActiveDataProvider;
use common\models\ShowTimesDetail;

/**
 * ShowTimesDetailSearch represents the model behind the search form about `common\models\ShowTimesDetail`.
 */
class ShowTimesDetailSearch extends ShowTimesDetail
{
    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id', 'cine_complex_id', 'show_times_id', 'screen_id', 'movie_id', 'playtime_id', 'normal_seat_remaining', 'vip_seat_remaining', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        // bypass scenarios() implementation in the parent class
        return Model::scenarios();
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public function search($params)
    {
        $query = ShowTimesDetail::find();

        // add conditions that should always apply here

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
        ]);

        $this->load($params);

        if (!$this->validate()) {
            // uncomment the following line if you do not want to return any records when validation fails
            // $query->where('0=1');
            return $dataProvider;
        }

        // grid filtering conditions
        $query->andFilterWhere([
            'cine_complex_id' => $this->cine_complex_id,
//            'show_times_id' => $this->show_times_id,
//            'screen_id' => $this->screen_id,
//            'movie_id' => $this->movie_id,
//            'playtime_id' => $this->playtime_id,
        ]);

        return $dataProvider;
    }
}

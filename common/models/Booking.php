<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\TimestampBehavior;
use yii\db\Query;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_booking".
 *
 * @property integer $id
 * @property string $full_name
 * @property string $phone_number
 * @property string $seat_code
 * @property string $complex_name
 * @property string $screen_name
 * @property integer $movie_title
 * @property integer $show_date
 * @property integer $show_time
 * @property integer $normal_ticket
 * @property integer $vip_ticket
 * @property integer $price
 * @property string $description
 * @property integer $status
 * @property integer $show_times_detail_id
 * @property integer $user_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property ShowTimesDetail $showTimes
 * @property User $user
 */
class Booking extends ActiveRecord
{
    public $pass_flag = false;

    const STATUS_BOOKED = 1;
    const STATUS_NOT_BOOKED = 0;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['full_name', 'phone_number', 'seat_code', 'complex_name', 'screen_name', 'movie_title', 'show_date', 'show_time', 'price'], 'required'],
            [['normal_ticket', 'vip_ticket', 'price', 'created_at', 'updated_at', 'status', 'show_times_detail_id'], 'integer'],
            [['movie_title', 'full_name', 'description'], 'string', 'max' => 255],
            [['seat_code', 'complex_name', 'screen_name', 'show_date', 'show_time'],'string', 'max' => 100],
            [['phone_number'], 'string', 'max' => 11],
            [['vip_ticket', 'normal_ticket', 'status'], 'default', 'value' => 0]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'full_name' => 'Full Name',
            'phone_number' => 'Phone Number',
            'seat_code' => 'Seat Code',
            'complex_name' => 'Complex Name',
            'screen_name' => 'Screen Name',
            'movie_title' => 'Movie Title',
            'show_date' => 'Show Date',
            'show_time' => 'Show Time',
            'normal_ticket' => 'Normal Ticket',
            'vip_ticket' => 'Vip Ticket',
            'price' => 'Price (VNĐ)',
            'description' => 'Description',
            'status' => 'Status',
            'show_times_detail_id' => 'ShowTimesID',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }

    public function getShowTimes()
    {
        return $this->hasOne(ShowTimesDetail::className(), ['id' => 'show_times_detail_id']);
    }

    public function formatDateTime($timestamp){
        return date('d/m/Y h:i:s A', $timestamp);
    }

    public function formatDate($timestamp){
        return date('d/m/Y', $timestamp);
    }

    public function formatTime($timestamp){
        return date('H:i', $timestamp);
    }

    public function formatNumber($number, $unit = 'VNĐ'){
        return number_format($number).$unit;
    }

    public function formatDataBeforeSave(){
        $showTimesDetail = new ShowTimesDetail();

        //get cinema complex name
        $showTimesDetail->cine_complex_id = $this->complex_name;
        $this->complex_name = $showTimesDetail->cineComplex->name;

        //get show date
        $showTimesDetail->show_times_id = $this->show_date;
        $this->show_date = date('Y/m/d',$showTimesDetail->showTimes->date);

        //get screen name
        $showTimesDetail->screen_id = $this->screen_name;
        $this->screen_name = $showTimesDetail->screen->name;

        //get movie title
        $showTimesDetail->movie_id = $this->movie_title;
        $this->movie_title = $showTimesDetail->movie->title;

        //get playtime
        $showTimesDetail->playtime_id = $this->show_time;
        $this->show_time = $this->formatTime($showTimesDetail->playtime->start_time). '-' .$this->formatTime($showTimesDetail->playtime->end_time);

        // Set Booking id from 5 field top
        $this->show_times_detail_id = $showTimesDetail::findOne([
            'cine_complex_id' => $showTimesDetail->cine_complex_id,
            'show_times_id' => $showTimesDetail->show_times_id,
            'screen_id' => $showTimesDetail->screen_id,
            'movie_id' => $showTimesDetail->movie_id,
            'playtime_id' => $showTimesDetail->playtime_id
        ])->id;
    }

    public function updateStatusSeat(){
        // Update status seat after save
        $listSeat =  array_map('intval', explode(',', Yii::$app->request->post()['seatChecked']));
        $bookingDetail = new BookingDetail();
        $id = $this->show_times_detail_id;

        $count = COUNT($listSeat);
        for ($i = 0 ; $i < $count; $i++) {
            $bookingDetail = $bookingDetail::findOne([
                'show_times_detail_id' => $id,
                'seat_id' => $listSeat["$i"]
            ]);
            $bookingDetail->status = $this::STATUS_BOOKED;
            $bookingDetail->save(false);
        }
    }

    public function resetStatusSeat(){
        // return status seat before in booking-detail
        $booking = Booking::findOne(['id' => $this->id]);
        $listSeatCode = $booking->seat_code;
        $listSeat =  explode(',', $listSeatCode);

        // Get seat line and sea code from List seat booking
        $idShowTimes = $booking->show_times_detail_id;
        $screen_id = $booking->showTimes->screen_id;
        $bookingDetail = new BookingDetail();
        $listConvert = [];
        foreach ($listSeat as $item):
            $line = substr($item, 0,1);
            $seatCode = substr($item, 1);
            $listConvert[$line][] = $seatCode;
        endforeach;

        // Query get id seat from list seat array convert
        $temp = [];
        foreach ($listConvert as $line => $seatCode):
            $query = new Query();
            $query->select(['s.id'])
                ->from('{{%seat}} s')
                ->join(
                    'inner join',
                    '{{%seat_line}} sl',
                    's.seat_line_id = sl.id' )
                ->where(['sl.line' => $line])
                ->andWhere(['in', 's.seat_code', $seatCode])
                ->andWhere(['s.screen_id' => $screen_id]);
            $query->createCommand();
            $temp['list_id'][] = $query->all();
        endforeach;

        // Filter id from list id of query
        $listId = [];
        foreach ($temp['list_id'] as $item):
            foreach ($item as $seatId):
                $listId[] = $seatId;
            endforeach;
        endforeach;

        $arrReturn = [];
        // Action update seat status by seat id in table booking detail
        $count = COUNT($listId);
        for ($i = 0 ; $i < $count; $i++) {
            $bookingDetail = $bookingDetail::findOne([
                'show_times_detail_id' => $idShowTimes,
                'seat_id' => $listId[$i]['id']
            ]);
            $bookingDetail->status = $this::STATUS_NOT_BOOKED;
            $bookingDetail->save(false);
            $arrReturn[] = $listId[$i]['id'];
        }

        return $arrReturn;
    }

    public function beforeSave($insert)
    {
        if(parent::beforeSave($insert)){  // When insert
            if($insert){
                if(!$this->pass_flag)
                    $this->formatDataBeforeSave();
            }else{  // When update
                $this->formatDataBeforeSave();

                $showTimes = ShowTimesDetail::findOne(['id' => $this->show_times_detail_id]);
                $booking = Booking::findOne(['id' => $this->id]);

                /*--- update qty to that show times ---*/
                $seatNormalBefore = $booking->normal_ticket; // old qty normal seat
                $seatVipBefore = $booking->vip_ticket; // old qty vip seat

                $seatNormalUpdate = $this->normal_ticket; // update qty normal seat
                $seatVipUpdate = $this->vip_ticket; // update qty vip seat

                $action = null;
                // Comparing Normal before and after
                if($seatNormalUpdate > $seatNormalBefore){
                    $newNormalSeat = $seatNormalUpdate - $seatNormalBefore;
                    $showTimes->normal_seat_remaining -= $newNormalSeat;
                    $action = 1;
                }
                if($seatNormalUpdate < $seatNormalBefore){
                    $newNormalSeat = $seatNormalBefore - $seatNormalUpdate;
                    $showTimes->normal_seat_remaining += $newNormalSeat;
                    $action = 1;
                }

                // Comparing Vip before and after
                if($seatVipUpdate > $seatVipBefore){
                    $newVipSeat = $seatVipUpdate - $seatVipBefore;
                    $showTimes->vip_seat_remaining -= $newVipSeat;
                    $action = 1;
                }
                if($seatVipUpdate < $seatVipBefore){
                    $newVipSeat = $seatVipBefore - $seatVipUpdate;
                    $showTimes->vip_seat_remaining += $newVipSeat;
                    $action = 1;
                }

                if ($action != null)
                    $showTimes->save(false);

                // Reset status seat
                // if seat code before same seat code after then no reset status
                if($booking->seat_code != $this->seat_code){
                    $this->resetStatusSeat();

                    /*--- update status new seat in booking detail ---*/
                    $this->updateStatusSeat();
                }
            }
            return true;
        }
        return false; // TODO: Change the autogenerated stub
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){ //When insert
            // Update qty seat in schedule after save
            $model = ShowTimesDetail::findOne(['id' => $this->show_times_detail_id]);
            $model->vip_seat_remaining -= $this->vip_ticket;
            $model->normal_seat_remaining -= $this->normal_ticket;
            $model->save(false);

            /*--- update status new seat in booking detail ---*/
            $this->updateStatusSeat();
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }

    /* Get List seat and seat line show in Screen when booking ticket */
    public function getListSeatById($id)
    {
        $listSeat = BookingDetail::find()->with('seat')->where(['show_times_detail_id' => $id])->asArray()->all();
        $listSeat =  ArrayHelper::getColumn($listSeat, 'seat_id');
        $listSeatWithLine = Seat::find()->with('seatLine')->where(['in', '{{%seat}}.id', $listSeat])->all();
        return ArrayHelper::map($listSeatWithLine, 'id', 'seat_code', 'seatLine.line');
    }

    public function beforeDelete()
    {
        // Update qty seat in schedule after delete
        $model = new ShowTimesDetail();
        $model = $model::findOne(['id' => $this->show_times_detail_id]);
        $model->vip_seat_remaining += $this->vip_ticket;
        $model->normal_seat_remaining += $this->normal_ticket;
        $model->save(false);

        // Update qty seat in schedule after delete
        $this->resetStatusSeat();

        return parent::beforeDelete(); // TODO: Change the autogenerated stub
    }
}

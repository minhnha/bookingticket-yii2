<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "tbl_booking_detail".
 *
 * @property integer $id
 * @property integer $show_times_detail_id
 * @property integer $seat_id
 * @property integer $status
 *
 * @property Booking $booking-ticket
 * @property Seat $seat
 */
class BookingDetail extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%booking_detail}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['show_times_detail_id', 'seat_id', 'status'], 'required'],
            [['show_times_detail_id', 'seat_id', 'status'], 'integer'],
            [['seat_id'], 'unique', 'targetAttribute' => ['show_times_detail_id', 'seat_id']],
            [['show_times_detail_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShowTimesDetail::className(), 'targetAttribute' => ['show_times_detail_id' => 'id']],
            [['seat_id'], 'exist', 'skipOnError' => true, 'targetClass' => Seat::className(), 'targetAttribute' => ['seat_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'show_times_detail_id' => 'Show Times Detail',
            'seat_id' => 'Seat ID',
            'status' => 'Status',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShowTimesDetail()
    {
        return $this->hasOne(ShowTimesDetail::className(), ['id' => 'show_times_detail_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeat()
    {
        return $this->hasOne(Seat::className(), ['id' => 'seat_id']);
    }
}

<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_show_times_detail".
 *
 * @property integer $id
 * @property integer $cine_complex_id
 * @property integer $show_times_id
 * @property integer $screen_id
 * @property integer $movie_id
 * @property integer $playtime_id
 * @property integer $normal_seat_remaining
 * @property integer $vip_seat_remaining
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Booking[] $bookings
 * @property CinemaComplex $cineComplex
 * @property User $createdBy
 * @property Movie $movie
 * @property Playtime $playtime
 * @property Screen $screen
 * @property ShowTimes $showTimes
 * @property User $updatedBy
 */
class ShowTimesDetail extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%show_times_detail}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['cine_complex_id', 'show_times_id', 'screen_id', 'movie_id', 'playtime_id'], 'required'],
            [['cine_complex_id', 'show_times_id', 'screen_id', 'movie_id', 'playtime_id', 'normal_seat_remaining', 'vip_seat_remaining'], 'integer'],
            [['playtime_id'], 'unique', 'targetAttribute' => ['cine_complex_id', 'show_times_id', 'screen_id', 'playtime_id'], 'message' => 'This Show times has already been taken.'],
            [['cine_complex_id'], 'exist', 'skipOnError' => true, 'targetClass' => CinemaComplex::className(), 'targetAttribute' => ['cine_complex_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['movie_id'], 'exist', 'skipOnError' => true, 'targetClass' => Movie::className(), 'targetAttribute' => ['movie_id' => 'id']],
            [['playtime_id'], 'exist', 'skipOnError' => true, 'targetClass' => Playtime::className(), 'targetAttribute' => ['playtime_id' => 'id']],
            [['screen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Screen::className(), 'targetAttribute' => ['screen_id' => 'id']],
            [['show_times_id'], 'exist', 'skipOnError' => true, 'targetClass' => ShowTimes::className(), 'targetAttribute' => ['show_times_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'cine_complex_id' => 'CineComplex',
            'show_times_id' => 'Show Times',
            'screen_id' => 'Screen',
            'movie_id' => 'Movie',
            'playtime_id' => 'Playtime',
            'normal_seat_remaining' => 'Normal Seat',
            'vip_seat_remaining' => 'Vip Seat',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['show_times_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCineComplex()
    {
        return $this->hasOne(CinemaComplex::className(), ['id' => 'cine_complex_id']);
    }
    public function getListCineComplex()
    {
        $list = CinemaComplex::find()->all();
        return ArrayHelper::map($list, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUserCreate(){
        return $this->createdBy->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getMovie()
    {
        return $this->hasOne(Movie::className(), ['id' => 'movie_id']);
    }
    public function getListMovie()
    {
        $list = Movie::find()->all();
        return ArrayHelper::map($list, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPlaytime()
    {
        return $this->hasOne(Playtime::className(), ['id' => 'playtime_id']);
    }
    public function getPlayTimeDetail()
    {
        return date('H:i', $this->playtime->start_time). ' - ' .date('H:i', $this->playtime->end_time);
    }
    public function getListPlaytime()
    {
        $list = Playtime::find()->select(['id','start_time', 'end_time'])->all();
        $listTemp = [];
        foreach ($list as $item):
            $listTemp[$item->id] = $this->formatTime($item->start_time). ' - ' .$this->formatTime($item->end_time);
        endforeach;
        return $listTemp;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScreen()
    {
        return $this->hasOne(Screen::className(), ['id' => 'screen_id']);
    }
    public function getListScreen()
    {
        $list = Screen::find()->all();
        return ArrayHelper::map($list, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShowTimes()
    {
        return $this->hasOne(ShowTimes::className(), ['id' => 'show_times_id']);
    }
    public function getShowTimesDate()
    {
        return $this->formatDate($this->showTimes->date);
    }
    public function getListShowTimes()
    {
        $list = ShowTimes::find()->all();
        $list = ArrayHelper::map($list, 'id', 'date');
        $listTemp = [];
        foreach ($list as $id => $date):
            $listTemp[$id] = $this->formatDate($date);
        endforeach;
        return $listTemp;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getUserUpdate(){
        return $this->updatedBy->full_name;
    }

    public function formatDateTime($timestamp){
        return date('d/m/Y h:i:s A', $timestamp);
    }

    public function formatDate($timestamp){
        return date('d/m/Y', $timestamp);
    }

    public function formatTime($timestamp){
        return date('H:i', $timestamp);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if($insert){
            // Update List seat to table booking_detail
            $listSeatByScreen = Seat::find()
                ->select('id')
                ->where(['screen_id' => $this->screen_id])
                ->all();
            $count = count($listSeatByScreen);
            $show_times_detail_id = $this->id;
            $listSeat = [];
            foreach ($listSeatByScreen as $key => $item):
                $listSeat[$key] = $item['id'];
            endforeach;

            for ($i = 0; $i < $count; $i++)
            {
                $model = new BookingDetail();
                $model->show_times_detail_id = $show_times_detail_id;
                $model->seat_id = $listSeat["$i"];
                $model->status = 0;
                if ($model->save(false)){

                }else{
                    echo '<pre>';
                    print_r($model->getErrors());die;
                }
            }
        }
        parent::afterSave($insert, $changedAttributes); // TODO: Change the autogenerated stub
    }
}

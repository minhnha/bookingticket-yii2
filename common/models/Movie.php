<?php

namespace common\models;

use Yii;
use yii\db\ActiveRecord;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_movie".
 *
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $cast
 * @property string $director
 * @property string $studio
 * @property integer $length
 * @property integer $version
 * @property string $content
 * @property integer $status
 * @property integer $cat_id
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property MovieCategory $cat
 * @property User $createdBy
 * @property User $updatedBy
 * @property ShowTimesDetail[] $showTimesDetails
 */
class Movie extends ActiveRecord
{
    CONST STATUS_SHOW = 0;
    CONST STATUS_COMING = 1;
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%movie}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'length', 'cat_id'], 'required'],
            ['version', 'required', 'message' => '{attribute} cannot be blank.'],
            [['title', 'version'], 'unique', 'targetAttribute' => ['title', 'version']],
            [['length', 'version', 'cat_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['content'], 'string'],
            [['title', 'cast', 'studio'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['director'], 'string', 'max' => 50],
            [['status'], 'integer', 'max' => 4],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => MovieCategory::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image' => 'Image',
            'cast' => 'Cast',
            'director' => 'Director',
            'studio' => 'Studio',
            'length' => 'Length (Minute)',
            'version' => 'Version',
            'content' => 'Content',
            'status' => 'Status',
            'cat_id' => 'Category',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(MovieCategory::className(), ['id' => 'cat_id']);
    }
    public function getCatTitle()
    {
        return $this->cat->title;
    }

    // Get list movie-category to _form
    public function getMovieList(){
        $movie_list = MovieCategory::find()->all();
        return ArrayHelper::map($movie_list, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUserCreate(){
        return $this->createdBy->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getUserUpdate(){
        return $this->updatedBy->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getShowTimesDetails()
    {
        return $this->hasMany(ShowTimesDetail::className(), ['movie_id' => 'id']);
    }

    // get category movie show in grid view of movie by cat_id
    public function getMovieCategory(){
        return $this->hasOne(MovieCategory::className(), ['id' => 'cat_id']);
    }

    public function getMovieByStatus($status = null)
    {
        if($status == Movie::STATUS_SHOW || $status == Movie::STATUS_COMING){
            $model = Movie::find()->select(['id', 'title', 'image', 'updated_at'])->where(['status' => $status])->all();
            return $model;
        }
        return null;
    }

    public function formatDateTime($timestamp){
        return date('d/m/Y h:i:s A', $timestamp);
    }

    public function formatDate($timestamp){
        return date('d/m/Y', $timestamp);
    }
}

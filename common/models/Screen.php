<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_screen".
 *
 * @property integer $id
 * @property string $name
 * @property integer $normal_seat_quantity
 * @property integer $vip_seat_quantity
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property Booking[] $bookings
 * @property CinemaComplex $cineComplex
 * @property Seat[] $seat
 * @property User $createdBy
 * @property User $updatedBy
 */
class Screen extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%screen}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'normal_seat_quantity', 'vip_seat_quantity', 'cine_complex_id'], 'required'],
            [['normal_seat_quantity', 'vip_seat_quantity', 'cine_complex_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['name'], 'string', 'max' => 100],
            [['cine_complex_id'], 'unique', 'targetAttribute' => ['name', 'cine_complex_id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'normal_seat_quantity' => 'Normal Seat Quantity',
            'vip_seat_quantity' => 'Vip Seat Quantity',
            'cine_complex_id' => 'Cinema Complex',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }
    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCineComplex(){
        return $this->hasOne(CinemaComplex::className(), ['id' => 'cine_complex_id']);
    }
    public function getNameCineComplex(){
        return $this->cineComplex->name;
    }
    public function getListCineComplex(){
        $list = CinemaComplex::find()->all();
        return ArrayHelper::map($list, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeats(){
        return $this->hasMany(Seat::className(), ['screen_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getBookings()
    {
        return $this->hasMany(Booking::className(), ['screen_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUserCreate(){
        return $this->createdBy->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getUserUpdate(){
        return $this->updatedBy->full_name;
    }

    public function formatDateTime($timestamp){
        return date('d/m/Y h:i:s A', $timestamp);
    }

    public function formatDate($timestamp){
        return date('d/m/Y', $timestamp);
    }
}

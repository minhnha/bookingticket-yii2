<?php

namespace common\models;

use Yii;
use yii\behaviors\BlameableBehavior;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "tbl_seat".
 *
 * @property integer $id
 * @property string $seat_code
 * @property integer $status
 * @property integer $cat_id
 * @property integer $seat_line_id
 * @property integer $screen_id
 * @property integer $created_at
 * @property integer $created_by
 * @property integer $updated_at
 * @property integer $updated_by
 *
 * @property SeatCategory $cat
 * @property User $createdBy
 * @property Screen $screen
 * @property SeatLine $seatLine
 * @property User $updatedBy
 */
class Seat extends ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%seat}}';
    }

    public function behaviors()
    {
        return [
            [
                'class' => TimestampBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
            ],
            [
                'class' => BlameableBehavior::className(),
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_by', 'updated_by'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_by'],
                ],
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['seat_code', 'cat_id', 'seat_line_id', 'screen_id'], 'required'],
            ['seat_code', 'unique', 'targetAttribute' => ['seat_code', 'screen_id', 'seat_line_id', 'cat_id']],
            [['seat_code', 'cat_id', 'seat_line_id', 'screen_id', 'created_at', 'created_by', 'updated_at', 'updated_by'], 'integer'],
            [['status'], 'string', 'max' => 4],
            [['screen_id', 'seat_line_id'], 'validateSeatLineQuantity'],
            [['screen_id', 'cat_id'], 'validateSeatCatQuantity'],
            [['cat_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeatCategory::className(), 'targetAttribute' => ['cat_id' => 'id']],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
            [['screen_id'], 'exist', 'skipOnError' => true, 'targetClass' => Screen::className(), 'targetAttribute' => ['screen_id' => 'id']],
            [['seat_line_id'], 'exist', 'skipOnError' => true, 'targetClass' => SeatLine::className(), 'targetAttribute' => ['seat_line_id' => 'id']],
            [['updated_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['updated_by' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'seat_code' => 'Seat Code',
            'status' => 'Status',
            'cat_id' => 'Seat Category',
            'seat_line_id' => 'Seat Line',
            'screen_id' => 'Screen',
            'created_at' => 'Created At',
            'created_by' => 'Created By',
            'updated_at' => 'Updated At',
            'updated_by' => 'Updated By',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCat()
    {
        return $this->hasOne(SeatCategory::className(), ['id' => 'cat_id']);
    }
    public function getCatTitle(){
        return $this->cat->title;
    }

    public function getListSeatCat()
    {
        $model = SeatCategory::find()->all();
        return ArrayHelper::map($model, 'id', 'title');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'created_by']);
    }
    public function getUserCreate(){
        return $this->createdBy->full_name;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getScreen()
    {
        return $this->hasOne(Screen::className(), ['id' => 'screen_id']);
    }
    public function getScreenName(){
        return $this->screen->name;
    }
    public function validateSeatCatQuantity(){
        $countBySeatCat = Seat::find()->where(['screen_id' => $this->screen_id])->andWhere(['cat_id' => $this->cat_id])->count();
        if($this->getCatTitle() == 'Normal'){
            if($countBySeatCat == $this->screen->normal_seat_quantity){
                $this->addError('cat_id', 'Normal Seat exceeds the permitted number in Screen has selected.');
            }
        }
        if($this->getCatTitle() == 'Vip'){
            if($countBySeatCat == $this->screen->vip_seat_quantity){
                $this->addError('cat_id', 'Vip Seat exceeds the permitted number in Screen has selected.');
            }
        }
    }
    public function getListScreen()
    {
        $model = Screen::find()->all();
        return ArrayHelper::map($model, 'id', 'name');
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getSeatLine()
    {
        return $this->hasOne(SeatLine::className(), ['id' => 'seat_line_id']);
    }

    public function getSeatLineName()
    {
        return $this->seatLine->line;
    }
    public function validateSeatLineQuantity(){
        $countBySeatLine = Seat::find()->where(['screen_id' => $this->screen_id])->andWhere(['seat_line_id' => $this->seat_line_id])->count();
        if($countBySeatLine == $this->seatLine->seat_quantity){
            $this->addError('seat_line_id', 'This SeatLine exceeds the permitted number in SeatLine has selected.');
        }
    }
    public function getListSeatLine()
    {
        $model = SeatLine::find()->all();
        return ArrayHelper::map($model, 'id', 'line');
    }

    public function getListStatus()
    {
        $model = Seat::find()->select('status')->asArray();
        echo '<pre>';
        foreach ($model as $item):
            print_r($item);
        endforeach;

        return $model;
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUpdatedBy()
    {
        return $this->hasOne(User::className(), ['id' => 'updated_by']);
    }
    public function getUserUpdate(){
        return $this->updatedBy->full_name;
    }

    public function formatDateTime($timestamp){
        return date('d/m/Y h:i:s A', $timestamp);
    }

    public function formatDate($timestamp){
        return date('d/m/Y', $timestamp);
    }

    public function getListSeatByScreen()
    {
        $listSeatWithLine = Seat::find()->with('seatLine')->all();
        return ArrayHelper::map($listSeatWithLine, 'id', 'seat_code', 'seatLine.line');
    }

}
